---
title: "To Lose the Best Way"
category: "Poster"
image: "kichot_thumb_2.jpg"
menu: projects
draft: true

images: 

  - ../images/kichot_poster.jpg
  - ../images/kichot_poster_2.png
---

It was great to work on a poster for a series of storytelling events *To lose the best way*. The initiative came from an organisation [Studnia O](http://studnia.org/) which gathers skillful storytellers and organises events like this one. The spotlight was being put on art of losing, and its greatest representative: Don Kichote.
