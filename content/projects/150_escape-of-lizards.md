---
title: "Escape of Lizards"
category: "Illustration"
image: "lizards_thumb.jpg"
menu: projects
draft: true

images:
  - ../images/escape_of_lizards_web2.jpg
---

An illustration that I've created for a competition. A metaphor for the main topic of the contest - Migration.
