---
title: "Formicarium"
category: "Game Design"
image: "formic_thumb.jpg"
menu: projects
images:
  - ../images/formic_grass.jpg
  - ../images/formic_char.png
  - ../images/formic_1.jpg
  - ../images/formic_2.jpg
  - ../images/formic_elements.png
  - ../images/formic_poster.jpg

---

{{<paragraph>}}Together with [Konrad Feiler](http://www.mathheartcode.com/), a skillful app-developer, we were working on a demo of a strategic simulation game [Formicarium](http://formicarium.org/). As an invisible hand the player can lead growing ant-hive through the impressive micro cosmos of a meadow. Unfortunatelly, we didn't reach the Kickstarter goal (but it was near!) and the project was frozen in the demo-phase. To be continued...Maybe!{{</paragraph>}}

{{<image_wide
  src="../images/formic_grass.jpg">}}
{{<image_double
  src-a="../images/formic_2.jpg"
  src-b="../images/formic_1.jpg">}}
{{<image_narrow
  src="../images/formic_char.png">}}
{{<image_narrow
  src="../images/formic_elements.png">}}
{{<image_wide
  src="../images/formic_poster.jpg">}}
