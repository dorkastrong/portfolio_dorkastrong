---
title: "Nedwang"
category: "Infographics"
image: "nedwang_thumb_2.jpg"
menu: projects
draft: true

images:
  - ../images/nedwang_2.jpg
  - ../images/nedwang_info.png

---

Nedwang is a Dutch company taking care of waste management. In collaboration with studio [Funk-e](https://www.funk-e.com/nl/) I've designed an infographics showing the flow of the waste in a city.
