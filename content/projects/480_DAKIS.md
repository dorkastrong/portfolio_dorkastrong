---
title: "DAKIS Impact Modeller"
category: "UX/UI"
image: "DAKIS-5-thumb.jpg"
menu: projects
---

{{<paragraph>}}In close collaboration with Johannes Stein I've developed the design prototype of the DAKIS Impact Modeller - a web-based tool for farmers and agricultural decision-makers.
The user is guided through assembling a network model based on chosen agricultural impact areas and drivers. The modelling process and the final showcase allow a user to understand better how her decisions affect other variables and their popularity in a specific target group.
From the other end, the scientists can gather the results and compare the models on a scale to better understand the needs of specific target groups.

The DAKIS Impact Modeller was developed for the project [DAKIS: Digital Agricultural Knowledge & Information System](https://adz-dakis.com/en/) - with headquarters in Müncheberg.{{</paragraph>}}

{{<video_vimeo id="817332338">}}
{{<image_wide
  src="../images/DAKIS-1.jpg">}}
{{<image_wide
  src="../images/DAKIS-2.jpg">}}    
{{<image_narrow
  src="../images/DAKIS-3.jpg">}}  
