---
title: "Airbus Space Exploration"
category: "Illustration / Explainer Video"
image: "airbus_thumb.jpg"
menu: projects
draft: false

  - ../images/airbus_vol1.jpg
  - ../images/airbus_vol2.jpg
  - ../images/airbus_vol3.jpg
  - ../images/airbus_vol6.jpg
  - ../images/airbus_vol7.jpg
  - ../images/airbus_vol8.jpg
  - ../images/airbus_bepi_1.jpg
  - ../images/airbus_bepi_2.jpg
  - ../images/airbus_bepi_3.jpg
  - ../images/airbus_bepi_5.jpg

youtube:
  - 60OGSWizndc
  - e2AXujdbW6U
  - l2gsQjb5Hfc
  - K80gHK4qCOE
---

{{<paragraph>}}Together with studio [Funk-e](https://www.funk-e.com/en/home-en/services/digital-media-creation/animation), we have created the Airbus Space Exploration explainer video series. The challenge was to communicate the information of recent missions of the European Space Agency & NASA in an easily accessible and attractive way. In this project, I was in charge of the visual style and all illustrations for the animated videos.{{</paragraph>}} 

{{<video_youtube id="60OGSWizndc">}}
{{<video_youtube id="e2AXujdbW6U">}}
{{<video_youtube id="l2gsQjb5Hfc">}}
{{<video_youtube id="K80gHK4qCOE">}}

{{<image_double
  src-a="../images/airbus_vol1.jpg"
  src-b="../images/airbus_vol2.jpg">}}
{{<image_double
  src-a="../images/airbus_vol3.jpg"
  src-b="../images/airbus_vol6.jpg">}}
{{<image_double
  src-a="../images/airbus_vol7.jpg"
  src-b="../images/airbus_vol8.jpg">}}
{{<image_double
  src-a="../images/airbus_bepi_1.jpg"
  src-b="../images/airbus_bepi_2.jpg">}}
{{<image_double
  src-a="../images/airbus_bepi_3.jpg"
  src-b="../images/airbus_bepi_5.jpg">}}




