---
title: "E-Waste Leaking From Europe"
category: "Infographics"
image: "info_e-waste_v2_thumb.jpg"
menu: projects
---

{{<paragraph>}}Infographic inspired by the experiment conducted by [Basel Action Network](https://www.ban.org/) and their report about illegal export of hazardous e-waste from Europe to non-OECD/EU countries, available under this [link](http://wiki.ban.org/images/f/f4/Holes_in_the_Circular_Economy-_WEEE_Leakage_from_Europe.pdf). This infographic is a passion project created to explore visual storytelling possibilities in information design and learn about the dramatic EU electronic waste problem.{{</paragraph>}}  

{{<image_narrow
  src="../images/info_e-waste_v2.jpg">}}