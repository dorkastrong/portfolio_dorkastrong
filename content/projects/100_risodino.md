---
title: "Risodino"
category: "Poster / Risograph"
image: "thumb_dino_3.jpg"
menu: projects
draft: true

images:
  - ../images/dino_1.jpg
  - ../images/dino_2.jpg
  - ../images/dino_3.jpg
---

After joining the project [Penthaus für schöne Formate](http://penthaus.space/) which is an open risograph printing studio, I've started to experiment with this technology. This time short one-illustration-story about old one, who is being shot with emotions. Big thanks to fluo-pink paint!
