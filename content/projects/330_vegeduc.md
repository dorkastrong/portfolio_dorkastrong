---
title: "Vegeduc Workshop"
category: "Workshop"
image: "330_vegeduc_thumb.jpg"
menu: projects
draft: true
images:
  - ../images/330_vegeduc_mock.jpg
  - ../images/330_cards_vegeduc_2.jpg
  - ../images/330_vegeduc_photo_1.jpg
  - ../images/330_vegeduc_photo_7.jpg
  - ../images/330_vegeduc_photo_2.jpg
  - ../images/330_vegeduc_photo_3.jpg
  - ../images/330_vegeduc_photo_5.jpg
  - ../images/330_vegeduc_photo_8.jpg
  - ../images/330_vegeduc_photo_6.jpg



---

{{<paragraph>}}Vegeduc is a project which is targeting school youth, aiming to build a consciousness about healthy nourishment and food origin. During [The Hive](https://www.hivers.fr/) creative residency, I've gotten invited to build a workshop around the topic of biodiversity. Together with [Amanda Lewis](https://www.linkedin.com/in/amanda-lewis-5610a08b/) and [Audrey Jaillard](http://audrey.jaillard.eu/), we've developed a scenario and custom made materials, to let kids discover the importance and richness of the insect world. Except co-designing a scenario I was mostly responsible for designing an insect card-game and a press-fit kit for assembling an insect house. The workshop was taking place during [BECOMEthecamp](https://thecamp.fr/en/news/when-teenagers-take-sustainability-their-own-hands ) program at [thecamp](https://thecamp.fr/en/place).{{</paragraph>}}

{{<image_narrow
  src="../images/330_vegeduc_mock.jpg">}}
{{<image_narrow
  src="../images/330_cards_vegeduc_2.jpg">}}
{{<image_narrow
  src="../images/330_vegeduc_photo_1.jpg">}}
{{<image_narrow
  src="../images/330_vegeduc_photo_7.jpg">}}
{{<image_narrow
  src="../images/330_vegeduc_photo_3.jpg">}}

{{<image_double
  src-a="../images/330_vegeduc_photo_2.jpg"
  src-b="../images/330_vegeduc_photo_5.jpg">}}
{{<image_double
  src-a="../images/330_vegeduc_photo_8.jpg"
  src-b="../images/330_vegeduc_photo_6.jpg">}}
