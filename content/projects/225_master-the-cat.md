
---
title: "Master the Cat"
category: "Illustration"
image: "cat_thumb.png"
menu: projects
draft: true
---

{{<paragraph>}}First design for a T-shirt line of my non-existing fashion brand. Made for a friend as a birthday present, in homage to his cat - Master. In reality he is the one who is controlling the matrix ♥{{</paragraph>}}

{{<image_narrow
  src="../images/cat_illu.png">}}
{{<image_narrow
  src="../images/tshirt_cat.jpg">}}
