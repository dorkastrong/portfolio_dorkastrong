---
title: "szajbus"
category: "Illustration"
image: "szajbus_thumb.jpg"
menu: projects
draft: true
images:
  - ../images/szajbus-center.png

---

Illustration for tailored Spotify playlist, that I've created for my sister's birthday. Dance!
