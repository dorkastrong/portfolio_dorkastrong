---
title: "Bonares Science Communication"
category: "Web Tools"
image: "430_Bonares_thumb.jpg"
menu: projects
---

{{<paragraph>}}Since 2020 together with Johannes Stein, we have been working on Science Communication and technological upgrade for [Bonares - the Center of Soil Research](https://www.bonares.de/). We have created multiple web tools to enable scientists to communicate their messages much quicker and more approachable. Our work resulted in multiple digital products and strategies. My responsibilities lay in the strategy concepts (together with Johannes), visual concepts, UX, UI, video animations, and illustration.

**Redesign and technological upgrade of Bonares Services Portal Web Platform:** 
In frames of the CMS, we've built multiple pre-defined page layouts respondent to the hierarchy of actual and possible content structure. On top of that, we have developed a variety of widgets - building blocks enabling the creation of various page configurations. We enhanced the content showcase for the target group switch - a functional widget that allows the creator to set up and the viewer to filter the content according to the target group.{{</paragraph>}} 

{{<image_wide
  src="../images/430_Bonares_Web_2.jpg">}}

{{<paragraph>}}**Infographic - Comparison Slider Tool:**
One of the most advanced widgets we've created is the comparison slider tool. The user can drag the bar to compare different image states and dive deeper into specific aspects by clicking on the points of interest. In the magnified view, one can use the bar again to compare the detailed images. This tool's creation process involved designing a custom illustrated infographic showcasing the soil compaction topic.{{</paragraph>}} 

{{<image_double
  src-a="../images/soil_assist_3.jpg"
  src-b="../images/soil_assist_4.jpg">}}

{{<image_double
  src-a="../images/soil_assist_2.jpg"
  src-b="../images/soil_assist_5.jpg">}}

{{<video_vimeo id="674398311">}}

{{<paragraph>}}**Scroll Presentation Tool:** 
Inspired by the scrolly-telling approach, we've created a page-type enabling quick construction of scroll-based presentations with pre-defined animated effects. In this case building blocks consist of different slide layouts, transitions and color schemes.{{</paragraph>}}

{{<video_vimeo id="675442224">}}

{{<paragraph>}}**Explainer Video Strategy:**
Another responsibility lay in developing a strategy to enable users (scientists) to create explainer videos independently. We have pre-selected the tools, defined the style guide, and developed building blocks. We picked a simple collaborative and browser-based software (https://invideo.io/) and uploaded the custom assets library (illustrated elements previously created by us). On top of that, we have created learning materials ([workflow tutorial](https://www.youtube.com/watch?v=_v1K9naUwB0), showcase file, and template file) so each user can take the project into their hands and work independently.{{</paragraph>}}

{{<image_wide
  src="../images/430_Bonares_assets_library.png">}}

{{<paragraph>}}**Explainer Videos:**
The explainer video strategy was preceded and based on three animated productions I've created for Bonares at the beginning of the project. In that way, I've learned about the needs and optimal workflow. It was also a chance to organically create the style guide for the future assets library.{{</paragraph>}} 

{{<video_vimeo id="564130816">}}
{{<video_vimeo id="564131587">}}

{{<image_double
  src-a="../images/bonares_1.jpg"
  src-b="../images/bonares_2.jpg">}}
{{<image_double
  src-a="../images/bonares_3.jpg"
  src-b="../images/bonares_5.jpg">}}
{{<image_double
  src-a="../images/bonares_11.jpg"
  src-b="../images/bonares_14.jpg">}}
{{<image_double
  src-a="../images/bonares_17.jpg"
  src-b="../images/bonares_8.jpg">}}
