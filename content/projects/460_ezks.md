---
title: "What Does the European Center for Kurdish Studies (EZKS) Do?"
category: "Illustration"
image: "460_EZKS_thumb.jpg"
menu: projects
---

{{<paragraph>}}This video was created for the Berlin-based NGO - the [European Center for Kurdish Studies (EZKS)](https://ezks.org/about-us/), to help explain its mission to the audience. In collaboration with [Karolin Nusa](https://www.karolinnusa.com) (concept, style and storyboard) and studio [Goldener Westen](https://www.goldenerwesten.net/), we have managed to communicate the institute’s vision in an expressive and accessible form. In this project, I was in charge of boardomatic and illustrations for the animation.{{</paragraph>}}

{{<video_vimeo id="723717788">}}

{{<image_double
  src-a="../images/460_EZKS_1.jpg"
  src-b="../images/460_EZKS_2.jpg">}}
{{<image_double
  src-a="../images/460_EZKS_3.jpg"
  src-b="../images/460_EZKS_5.jpg">}}
{{<image_double
  src-a="../images/460_EZKS_6.jpg"
  src-b="../images/460_EZKS_7.jpg">}}
{{<image_double
  src-a="../images/460_EZKS_8.jpg"
  src-b="../images/460_EZKS_9.jpg">}}
{{<image_double
  src-a="../images/460_EZKS_10.jpg"
  src-b="../images/460_EZKS_11.jpg">}}
