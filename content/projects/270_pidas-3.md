---
title: "Pidas Benchmark"
category: "Ilustration"
image: "pidas_thumb.png"
menu: projects
draft: true
---

{{<paragraph>}}It's been the third time that I've designed the set of illustrations for the publication *Pidas Benchmark Studie*. [Pidas](https://www.pidas.com/) takes care of customer experience and service with a focus on automation. The brochure got designed by studio [Goldener Westen](https://www.goldenerwesten.net/) from Berlin.{{</paragraph>}}

{{<image_narrow
  src="../images/pidas_3_1.jpg">}}
{{<image_narrow
  src="../images/pidas_3_2.jpg">}}
{{<image_narrow
  src="../images/pidas_3_3.jpg">}}
{{<image_narrow
  src="../images/pidas_3_4.jpg">}}