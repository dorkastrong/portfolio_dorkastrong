---
title: "Konch"
category: "Urban Element"
image: "konch_thumb_2.jpg"
menu: projects
---

{{<paragraph>}}Together with [Krisjanis Rijnieks](https://rijnieks.com/), [Leda Vaneva](https://ledavaneva.com/), and Ranjit Menon, we've developed this project in frames of the FURNISH program. Konch is an open-source multipurpose urban design element that can be made in a standard fab lab. It creates safe settings for discussion or relaxation in the Covid-19 situation. The audio system integrated into a comfortable seat works like a big scale "walkie-talkie," enabling organic, screen-less conversations while keeping the social distance.

**Check out the project on** https://konch.info/{{</paragraph>}}

{{<image_wide
  src="../images/konch_11.jpeg">}}

{{<image_wide
  src="../images/konch_77.jpeg">}}

{{<image_wide
  src="../images/konch_2.jpeg">}}

{{<image_double
  src-a="../images/konch_3.jpg"
  src-b="../images/konch_4.jpg">}}    

{{<image_wide
  src="../images/konch_6.jpeg">}} 

{{<image_wide
  src="../images/konch_8.jpg">}} 