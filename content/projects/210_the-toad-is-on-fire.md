---
title: "The Toad Is on Fire"
category: "Interactive Animation"
image: "toad_anim_thumb.jpg"
menu: projects
draft: true
---

{{<paragraph>}}!!!! CLICK ON THE TOAD !!!!	

My first interactive animation project! Fully based on HTML5 and Javascript, so it works very nice in the browser and it's easy to integrate on the website. Credits to [Kris](https://rijnieks.com/) for Javascript support.

The idea for this small animated story came from the time I've spend on researching the topic for the [toads-zine](https://dorotaorlof.com/projects/220_toads-zine/). Enjoy clicking!{{</paragraph>}}  

<iframe src="/animations/frog/frog_steam_anim_20_with-code.html" frameborder="no" onload="resizeIframe(this)"></iframe>
