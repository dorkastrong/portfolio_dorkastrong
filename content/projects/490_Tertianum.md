---
title: "Tertianum"
category: "Illustration"
image: "Tertianum-2-thumb.jpg"
menu: projects
---

{{<paragraph>}}Together with [Goldener Westen](https://goldenerwesten.com), we developed an explainer video for [Tertianum](https://tertianum-premiumresidences.de/berlin/) - premium senior residencies located in major cities in Germany. Tertianum needed help finding staff for their locations - the applicants could get intimidated by the company's exclusive and distanced external communication. They wanted to appear like this to possible future residents, still for the staff applicants; they wanted to reveal their casual  "backstage" atmosphere. I've developed a style and a set of illustrations that got animated into a beautiful video with a nice narrational twist. {{</paragraph>}}

{{<image_double
  src-a="../images/Tertianum-01.jpg"
  src-b="../images/Tertianum-02.jpg">}}
{{<image_wide
  src="../images/Tertianum-03.jpg">}}
{{<image_double
  src-a="../images/Tertianum-05.jpg"
  src-b="../images/Tertianum-06.jpg">}}
{{<image_wide
  src="../images/Tertianum-07.jpg">}}
{{<image_double
  src-a="../images/Tertianum-09.jpg"
  src-b="../images/Tertianum-12.jpg">}}
{{<image_wide
  src="../images/Tertianum-13.jpg">}}
{{<image_double
  src-a="../images/Tertianum-19.jpg"
  src-b="../images/Tertianum-18.jpg">}}
{{<image_wide
  src="../images/Tertianum-17.jpg">}}
{{<image_double
  src-a="../images/Tertianum-23.jpg"
  src-b="../images/Tertianum-24.jpg">}}
{{<image_wide
  src="../images/Tertianum-21.jpg">}}
