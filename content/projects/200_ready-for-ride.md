---
title: "Ready for a Ride"
category: "Illustration"
image: "ride_thumb.jpg"
menu: projects
images:
  - ../images/ride_2.png
  - ../images/ride.png
---

{{<paragraph>}}A personal project, inspired by the favorite sci-fi book, and the old car of my parents. Bam!{{</paragraph>}}

{{<image_narrow
  src="../images/ride_2.png">}}
{{<image_narrow
  src="../images/ride.png">}}

