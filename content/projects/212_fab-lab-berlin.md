---
title: "Fab Lab Berlin"
category: "Illustration / Animation"
image: "fablab_thumb.jpg"
menu: projects
draft: true
---

{{<paragraph>}}It was fun to work on the illustrations for a new website of [Fab Lab Berlin](https://www.fablab.berlin/). I've created sets of icons for the Machine and Events section and animation for the landing page. At the end only reduced version of the animation appeared on the website, but still I'm proud of the complex one.{{</paragraph>}}

{{<video_vimeo id="290866859">}}

{{<image_wide
  src="../images/fablab_events2.jpg">}}
{{<image_wide
  src="../images/fablab_machines.jpg">}}
