---
title: "The Flower of the Fern"
category: "Illustration"
image: "360_kwiat_thumb.jpg"
menu: projects
images:
---

{{<paragraph>}}[Studio Myra](https://www.myra.world/en/) reached out to me asking for illustrating one of the Slavic legends. As I could choose which one I went for one that I remember most vividly and emotionally from my childhood - The Legend About the Flower of the Fern. First, I've designed one style frame which is showing the atmosphere and aesthetics. To be continued soon!{{</paragraph>}}  

{{<image_narrow
  src="../images/kwiat_paproci_12.png">}}







