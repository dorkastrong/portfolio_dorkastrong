---
title: "I'm Not Shaving"
category: "Interactive Installation"
image: "shave_thumb.jpg"
menu: projects
images:
  - ../images/shave_2.JPG
  - ../images/shave_4.jpg
  - ../images/fin_all_2.jpg
  - ../images/fin_all_1.jpg
---

{{<paragraph>}}I'm not shaving is my final [Fab Academy](http://fabacademy.org/) project. It's an interactive installation: capacitance sensor (built from felt and conductive thread) which is triggering an animation being displayed from a projector. Everything runs on Raspberry Pi.

For more details, check my [final project documentation](http://fab.academany.org/2018/labs/barcelona/students/dorota-orlof/final_project/), and if you want to get to know what else I was doing (a lot!) during the Fab Academy check my [Fab Academy website](http://fab.academany.org/2018/labs/barcelona/students/dorota-orlof/).{{</paragraph>}}

{{<video_vimeo id="290940517">}}
{{<video_vimeo id="290925946">}}

{{<image_double
  src-a="../images/shave_2.JPG"
  src-b="../images/shave_4.jpg">}}
{{<image_double
  src-a="../images/fin_all_2.jpg"
  src-b="../images/fin_all_1.jpg">}}