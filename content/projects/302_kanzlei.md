---
title: "Kanzlei der Zukunft"
category: "Illustration"
image: "haufe_infographic_thumb_2.jpg"
menu: projects

---

{{<paragraph>}}In collaboration with [Goldener Westen](https://www.goldenerwesten.net/) I have created a modular illustration representing the topic "Office of the Future"

{{</paragraph>}}

{{<image_narrow
  src="../images/haufe_infographic_1.jpg">}}
{{<image_double
  src-a="../images/haufe_infographic_2_2.jpg"
  src-b="../images/haufe_infographic_3_3.jpg">}}
{{<image_double
  src-a="../images/haufe_infographic_4_4.jpg"
  src-b="../images/haufe_infographic_5_5.jpg">}}
{{<image_double
  src-a="../images/haufe_infographic_2.jpg"
  src-b="../images/haufe_infographic_3.jpg">}}
{{<image_double
  src-a="../images/haufe_infographic_4.jpg"
  src-b="../images/haufe_infographic_5.jpg">}}
