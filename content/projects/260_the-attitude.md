---
title: "The Attitude"
category: "Music Video Concept"
image: "the_attitude_thumb.jpg"
menu: projects
draft: true
---

{{<paragraph>}}Together with [Francesco Garbo](http://www.francescogarbo.com/) I've created a concept for a music video for [Romi](https://www.instagram.com/romiofficial96/) a Namibian raper, who was releasing her new piece "The Attitude" at the beginning of 2020. Check out this girl!{{</paragraph>}}

{{<video_youtube id="gx3ZBjW7jLQ">}}

{{<image_double
  src-a="../images/the_attitude_1.jpg"
  src-b="../images/the_attitude_2.jpg">}}  
