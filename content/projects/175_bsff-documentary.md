---
title: "BSFF Documentary"
category: "Illustration"
image: "docu_thumb.jpg"
menu: projects
draft: true
images:
  - ../images/documntary_1.jpg
---

{{<paragraph>}}BSFF ia a [Berlin Student Film Festival](https://www.berlinstudentfilmfestival.com/about/). The topics are divided into twelve categories. I've gotten asked to create an artwork for one of them - Documentary. This is why the motives on illustration are the ones which are being associated with popular topics of documentary programs: pre-historic era, religion, outer-space...but together they create an absurdly constellation.{{</paragraph>}} 

{{<image_wide
  src="../images/documntary_1.jpg">}}