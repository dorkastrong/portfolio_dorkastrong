---
title: "MotionLab Berlin"
category: "Illustration"
image: "motionlab_thumb.jpg"
menu: projects
weight: 24
images:
  - ../images/motionlab_01.jpg
  - ../images/motionlab_02.jpg
  - ../images/motionlab_03.jpg
  - ../images/motionlab_04.jpg
  - ../images/motionlab_05.jpg
  - ../images/motionlab_06.jpg
  - ../images/motionlab_08.jpg
  - ../images/motionlab_09.jpg
  - ../images/motionlab_10.jpg
draft: true
vimeo:
  - 404043688
---

{{<paragraph>}}This time I've got asked by [MotionLab Berlin](https://motionlab.berlin/) to develop the set of illustrations and icons as part of their branding-redesign. Motionlab is a big space dedicated to assisting prototyping and digital manufacturing.

The starting point for me was their new logo, solid and strongly based on an isometric grid. The challenge was to show in a visual way the variety and different aspects of MotionLabs service.{{</paragraph>}}

{{<video_vimeo id="404043688">}}

{{<image_wide
  src="../images/motionlab_01.jpg">}}

{{<image_double
  src-a="../images/motionlab_02.jpg"
  src-b="../images/motionlab_03.jpg">}}
{{<image_double
  src-a="../images/motionlab_04.jpg"
  src-b="../images/motionlab_05.jpg">}}
{{<image_double
  src-a="../images/motionlab_06.jpg"
  src-b="../images/motionlab_08.jpg">}}
{{<image_wide
  src="../images/motionlab_09.jpg">}}
{{<image_wide
  src="../images/motionlab_10.jpg">}}
