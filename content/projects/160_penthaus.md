---
title: "Penthaus"
category: "Visual Identity"
image: "penthaus_thumb.png"
menu: projects
draft: true
images:
  - ../images/banane_web_3.jpg
  - ../images/banane_web_4.jpg
---

{{<paragraph>}}I've developed an illustrative visual identity for the initiative that I'm part of - [Penthaus fuer schoene formate](http://penthaus.space/). It's an open risographie atelier located in Berlin-Neukoelln. The main motive comes from the template page from Risographie Machine which is made from....banana fiber! During the opening event banana was also waving his foot from the projected illustration.{{</paragraph>}}

{{<image_narrow
  src="../images/banane_web_3.jpg">}}
{{<video_vimeo id="291294198">}}



