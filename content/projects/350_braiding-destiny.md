---
title: "Braiding Destiny"
category: "Visual Journalism"
image: "350_braiding_thumb.jpg"
menu: projects
---

{{<paragraph>}}Braiding Destiny is an experimental Visual Journalism project. Together with Sophie Kerkhof we've designed a room-experience, which is aiming to put under the spotlight the case of trafficking of Nigerian underage women in the Western European sex market.
During the research we've got to know about one Sicilian hairdressing salon, which symbolically let in the streak of hope into this story. This became an inspiration for our room-experience, where the spectator is learning about the case in an unconventional way... in our hair salon! The event is going to take place, hopefully this year, after summer

The project was designed in collaboration with [VersPers](https://www.verspers.nl/), [Lost in Europe](https://lostineurope.org/) and [ACED](https://www.aced.site/).{{</paragraph>}}

{{<image_narrow
  src="../images/350_braiding_1.jpg">}}
{{<image_narrow
  src="../images/350_braiding_spectator.jpg">}}
{{<image_narrow
  src="../images/350_braiding_faces-01.png">}}
{{<image_narrow
  src="../images/350_braiding_magazine.jpg">}}






