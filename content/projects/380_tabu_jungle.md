---
title: "Tabu Jungle"
category: "Online Event"
image: "Tabu_Jungle_portf_thumb.jpg"
menu: projects
images:

  - ../images/Tabu_Jungle_portf_0.jpg
  - ../images/Tabu_Jungle_portf_1.jpg
  - ../images/Tabu_Jungle_portf_2.jpg
  - ../images/Tabu_Jungle_portf_5.png
  - ../images/Tabu_Jungle_portf_6.jpg

vimeo:
  - 563594031
  
---

{{<paragraph>}}Tabu Jungle is an experimental online event aiming to initiate a conversation around sexual health, which is still too often kept silent. Together with Yair Kira and Marie Dietzle, we've designed an online experience presented at the [People to People Playground](https://www.dizf.de/english/network/network-events/people-to-people-2021.html) festival in May 2021. We created and shared the survey, which helped us understand which topics lacked conversation. Based on that information, I've created a complex illustration depicting underrepresented subjects symbolically and playfully. This illustration served as a map - a visual background for the user avatars during the event. The participants could gather around a specific area of the image and discuss the topics that interest them. {{</paragraph>}}

{{<image_narrow
  src="../images/Tabu_Jungle_portf_0.jpg">}}
{{<image_narrow
  src="../images/Tabu_Jungle_portf_1.jpg">}}
{{<image_narrow
  src="../images/Tabu_Jungle_portf_2.jpg">}}

{{<paragraph>}}We have set up an interactive exhibition space on the platform Gather. Visitors were invited to explore the "jungle" of illustrations and chat about possible interpretations of the visuals, as it often happens during traditional vernissages.{{</paragraph>}}

{{<image_double
  src-a="../images/Tabu_Jungle_portf_5.png"
  src-b="../images/Tabu_Jungle_portf_6.jpg">}}












  