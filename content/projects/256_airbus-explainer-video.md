---
title: "Airbus"
category: "Illustration / Explainer Video"
image: "airbus2_thumb.jpg"
menu: projects
images:
  - ../images/airbus_2Artboard6.jpg
  - ../images/airbus_2Artboard7_2.jpg
  - ../images/airbus_2Artboard8.jpg
  - ../images/airbus_2Artboard9.jpg
  - ../images/airbus_2Artboard3.jpg
  - ../images/airbus_2-11.jpg
  - ../images/airbus_2-10.jpg
  - ../images/airbus_2Artboard10.jpg
  - ../images/airbus_2Artboard11.jpg
---

{{<paragraph>}}In cooperation with animation studio Funk-e we've created a series of futuristic explainer videos for Airbus. I was responsible for creating the style and all the vector illustrations which were animated afterwards: backgrounds, characters, elements.

You can watch a Discovery Space channel with all explainer videos [here](https://www.youtube.com/playlist?list=PLVwi8znytoZJ13qPUf0OJql5ZFhkY_2ZC).{{</paragraph>}}

{{<image_wide
  src="../images/airbus_2Artboard7_2.jpg">}}
{{<image_double
  src-a="../images/airbus_2Artboard6.jpg"
  src-b="../images/airbus_2Artboard8.jpg">}}
{{<image_double
  src-a="../images/airbus_2Artboard9.jpg"
  src-b="../images/airbus_2Artboard3.jpg">}}
{{<image_double
  src-a="../images/airbus_2-11.jpg"
  src-b="../images/airbus_2-10.jpg">}}
{{<image_double
  src-a="../images/airbus_2Artboard10.jpg"
  src-b="../images/airbus_2Artboard11.jpg">}}