---
title: "It never drops"
category: "Illustration"
image: "Inflation_thumb.jpg"
menu: projects
draft: true

---

{{<paragraph>}}My exploration of illustration styles. I imagine this illustration to be a symbolic depiction of extreme inflation, which is happened in Poland from the beginning of 2021 onwards.{{</paragraph>}}

{{<image_narrow
  src="../images/Inflation.jpg">}}
