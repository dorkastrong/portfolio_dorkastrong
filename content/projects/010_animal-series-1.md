---
title: "Animals Secrets"
category: "Illustration / Personal Project"
image: "dolphin_thumb.jpg"
menu: projects
draft: true

images:

  - ../images/dolphin.jpg
  - ../images/crocodile.jpg
  - ../images/ayeaye.jpg
  
---

Personal project, which is a series of illustrations for a book about crazy features or activities of animals. I've started this initiative thinking about my newly born niece. I'm a bit afraid she will grow up before I will finish.
