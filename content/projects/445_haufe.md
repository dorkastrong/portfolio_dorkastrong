---
title: "Learning Management System"
category: "Illustration"
image: "LMS_thumb_2.jpg"
menu: projects

---

{{<paragraph>}}Together with [Goldener Westen](https://www.goldenerwesten.net/) we have created an explainer video presenting the idea of the new product of Haufe Group: Learning Management System.  
In this project, I was in charge of the visual style, storyboard and illustrations for the animation.{{</paragraph>}}

{{<video_vimeo id="723718504">}}

{{<image_narrow
  src="../images/LMS_2.jpg">}}  
{{<image_double
  src-a="../images/LMS_3.jpg"
  src-b="../images/LMS_4.jpg">}}
{{<image_double
  src-a="../images/LMS_5.jpg"
  src-b="../images/LMS_6.jpg">}}  
{{<image_double
  src-a="../images/LMS_8.jpg"
  src-b="../images/LMS_9.jpg">}}
{{<image_narrow
  src="../images/LMS_11.jpg">}}
{{<image_double
  src-a="../images/LMS_12.jpg"
  src-b="../images/LMS_13.jpg">}}
      