---
title: "Paper Under Control"
category: "Art Project"
image: "puc_thumb2.jpg"
menu: projects
images:
  - ../images/puc_art_01.jpg
  - ../images/puc_art_01a.jpg
  - ../images/puc_art_02.jpg
  - ../images/puc_art_03.jpg
  - ../images/puc_art_04.jpg
  - ../images/puc_art_04a.jpg
  - ../images/puc_art_05.jpg
  - ../images/puc_art_06.jpg
  - ../images/puc_art_07a.jpg
  - ../images/puc_wer_01.jpg
  - ../images/puc_wer_02.jpg
  - ../images/puc_wer_03.jpg
  - ../images/puc_wer_04.jpg
  - ../images/puc_wer_05.jpg
  - ../images/puc_wer_06.jpg
  - ../images/puc_wer_07.jpg
---

{{<paragraph>}}What would Gutenberg do if he had a design software? Paper Under Control is a project that combines the aesthetics of old school printing technique with the versatility of contemporary digital softwares.

Together with [Eitan Rieger](https://www.rieger.design/) we've joined forces in order to embark on a journey of developing a new medium that combines both old and new. Literally speaking: by using different digital softwares we've designed 14 artistic images, which by the use of laser cutter got engraved in the wooden plates. That's how we got the templates to use with the self-made printing press, and imprint them on the high-quality paper.

The result of this process was the exhibition *Paper under control* shown at the Greenhouse Berlin between 10-19 March 2017.{{</paragraph>}}

{{<image_narrow
  src="../images/puc_art_01.jpg">}}
{{<image_narrow
  src="../images/puc_art_01a.jpg">}}
{{<image_narrow
  src="../images/puc_art_05.jpg">}}
{{<image_narrow
  src="../images/puc_art_04a.jpg">}}
{{<image_double
  src-a="../images/puc_art_03.jpg"
  src-b="../images/puc_art_02.jpg">}}
{{<image_double
  src-a="../images/puc_art_06.jpg"
  src-b="../images/puc_art_07a.jpg">}}
{{<image_double
  src-a="../images/puc_wer_01.jpg"
  src-b="../images/puc_wer_02.jpg">}}
{{<image_double
  src-a="../images/puc_wer_03.jpg"
  src-b="../images/puc_wer_04.jpg">}}
{{<image_double
  src-a="../images/puc_wer_05.jpg"
  src-b="../images/puc_wer_06.jpg">}}
