---
title: "Polenallergie 2019"
category: "Poster"
image: "polenallergie4_thumb.jpg"
menu: projects
images:
  - ../images/polenallergie4.jpg
  - ../images/polenallergie_poster.png
---

{{<paragraph>}}Another year I had a chance to work on a visual site of [Polenallergie](http://www.polenallergie.de/). It's a yearly polish jazz festival taking place in Nurenberg, where Krakauer Haus is located.{{</paragraph>}} 

{{<image_narrow
  src="../images/polenallergie4.jpg">}}

{{<image_narrow
  src="../images/polenallergie_poster.png">}}
