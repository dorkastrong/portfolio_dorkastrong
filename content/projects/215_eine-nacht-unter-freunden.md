---
title: "Eine Nacht unter Freunden"
category: "Illustration"
image: "kobalt_thumb.jpg"
menu: projects
draft: true
images:
  - ../images/kobalt_1.jpg
  - ../images/kobalt_3.jpg
  - ../images/kobalt_2.jpg
  - ../images/kobalt_6.jpeg
  - ../images/kobalt_5.jpeg
---

{{<paragraph>}}I was collaborating with [Lennart Suetterlin](http://lennart-suetterlin.de/by) on a design of an invitation for the 20-years anniversary event for studio Kobalt. My task was to create the illustration for the front side of the invitation. The topic was: *One night among friends* with the suggestion of a Halloween-like party. It was super fun to draw all the weirdos and monsters! Monsters are the best.{{</paragraph>}}

{{<image_narrow
  src="../images/kobalt_1.jpg">}}
{{<image_narrow
  src="../images/kobalt_3.jpg">}}
{{<image_double
  src-a="../images/kobalt_6.jpeg"
  src-b="../images/kobalt_5.jpeg">}}
