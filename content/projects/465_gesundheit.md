---
title: "Gesundheit in Deutschland"
category: "Illustration"
image: "RKI_thumbb.jpg"
menu: projects

---

{{<paragraph>}}Another enjoyable collaboration with the studio [Goldener Westen](https://www.goldenerwesten.net/) from Berlin. This time we've created the explainer video about the project of Robert Koch Institute, which is currently under development: a platform helping to predict upcoming health crises in Germany.  
In this project I was in charge of the storyboard, visual style, and vector illustrations.{{</paragraph>}}

{{<video_vimeo id="723718388">}}
  
{{<image_narrow
  src="../images/RKI_15.png">}}
{{<image_double
  src-a="../images/RKI_13.png"
  src-b="../images/RKI_11.png">}}
{{<image_double
  src-a="../images/RKI_6.png"
  src-b="../images/RKI_2.png">}}
  {{<image_double
  src-a="../images/RKI_12.png"
  src-b="../images/RKI_3.png">}}
  {{<image_narrow
  src="../images/RKI_4.png">}}