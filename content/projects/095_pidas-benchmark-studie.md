---
title: "Pidas Benchmark Studie"
category: "Illustration"
image: "pidas_thumb2.jpg"
menu: projects
draft: true

images:
  - ../images/pidas_3.jpg
  - ../images/pidas_1.jpg
  - ../images/pidas_2.jpg
  - ../images/pidas_cover.jpg
  - ../images/omnichanelling.jpg
  - ../images/service_automation.jpg
  - ../images/fans.jpg
  - ../images/service_design.jpg
  - ../images/service_transformation.jpg
---

I've designed the set of illustrations for the publication *Pidas Benchmark Studie*. The brochure got designed by studio [Goldener Westen](https://www.goldenerwesten.net/) from Berlin.
