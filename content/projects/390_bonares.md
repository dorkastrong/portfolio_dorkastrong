---
title: "Bonares"
category: "Explainer Videos"
image: "bonares_assessment_thumb_2.jpg"
menu: projects
draft: true

vimeo:
  - 564130816
  - 564131587
images:
  - ../images/bonares_18.png
  - ../images/bonares_19.png
---

For this project, I've taken the role of art director, executor, and producer, which were many responsibilities but very satisfying! I've created two animated intro videos presenting ideas of two projects, which are part of [Bonares Centre](https://www.bonares.de/): [Bonares Repository](https://maps.bonares.de/mapapps/resources/apps/bonares/index.html?lang=en) and [Assessment Platform](https://www.bonares.de/socioeconomics/sustainability-assessment). Together with scientists from these projects, we've written scripts that created the base for animated stories.

Bonares is a platform that gathers and communicates the research from soil and agriculture-related domains. It provides tools for scientists to conduct the research, collect and store the data and showcase the results.
Since 2020 together with Johannes Stein we are working on redesigning the platform and enhancing the Bonares science communication. The project aims to build a set of tools, elements, and frameworks that scientists can reuse later on.
