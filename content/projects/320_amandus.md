---
title: "Amandus"
category: "Visual Identity"
image: "amandus_thumb.jpg"
menu: projects
---

{{<paragraph>}}I've got reached out from studio [Upstruct](http://www.upstruct.com/) to create a new visual identity for [Amandus – Lillehammer International Student Film Festival](https://amandusfestivalen.no/en/). Simultaneously with developing the identity I was working on a festival poster - which became a reveal of the new logo-inspiration: a film reel-comet.{{</paragraph>}} 

{{<image_narrow
  src="../images/amandus_poster_1.jpg">}}
{{<image_narrow
  src="../images/amandus_poster_3.png">}}
{{<image_narrow
  src="../images/amandus_logo.jpg">}}
{{<image_narrow
  src="../images/amandus_comp.jpg">}}