---
title: "Dream From the Courtyard"
category: "Animation / Personal Project"
image: "manarola_anim2_thumb.jpg"
menu: projects
draft: true
---

{{<paragraph>}}Personal project in a memory of the holidays a few years ago. Courtyard of the grandmas house, Genova. Church bell is echoing, the concrete floor is steaming after the hot day and fish smells from the nearby store. You, me, a few friends and a small one.{{</paragraph>}}

<iframe src="/animations/manarola_3_animation/manarola_fire.html" frameborder="no" onload="resizeIframe(this)"></iframe>
