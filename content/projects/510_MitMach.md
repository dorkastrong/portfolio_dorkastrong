---
title: "MitMach-Werkstatt"
category: "Illustration"
image: "Fablab-v2-thumb.jpg"
menu: projects
---

{{<paragraph>}} I've developed a set of illustrations for the opening of [Mitmach-Werkstatt](https://www.instagram.com/mitmach.werkstatt/) - a kid's maker space in Bad Belzig, initiated by the NGO [Neuland21](https://neuland21.de). It was great to be back in the fab lab, at least mentally! The illustrations create a sleek whole with a great logotype developed by [Annika Paetsch](https://annikapaetsch.myportfolio.com). Let the workshop be used happily! {{</paragraph>}}

{{<image_wide
  src="../images/Fablab-v2-5.png">}}
{{<image_double
  src-a="../images/Fablab-v2-1.png"
  src-b="../images/Fablab-v2-2.png">}}
{{<image_double
  src-a="../images/Fablab-v2-3.png"
  src-b="../images/Fablab-v2-4.png">}}
