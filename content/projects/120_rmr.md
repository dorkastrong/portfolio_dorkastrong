---
title: "rmr"
category: "Visual Identity"
image: "thumb_rmr.jpg"
menu: projects
draft: true

images:
  - ../images/rmr_logo_2.png
  - ../images/rmr_branding2.png
---

Visual Identity developed for the Legal Counsel Office - *rmr*.
