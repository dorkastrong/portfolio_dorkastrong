---
title: "The Secret of the Open Fortress"
category: "Ilustration"
image: "zmosc_thumb.png"
menu: projects
draft: true
images:
  - ../images/zmosc_rynek.jpg
  - ../images/zmosc_kamienice.jpg
  - ../images/zmosc_parasole.jpg
  - ../images/zamosc_plan.png


---
{{<paragraph>}}
*The Secret of the Open Fortress* is a playful children's book written by Izabela Winiewicz. The adventure story is aiming to transfer a knowledge about the history of a World Heritage City - Zamość. I've illustrated the text, and also designed a stylised map of the city.{{</paragraph>}}

{{<image_narrow
  src="../images/zmosc_rynek.jpg">}}
{{<image_narrow
  src="../images/zmosc_kamienice.jpg">}}
{{<image_narrow
  src="../images/zmosc_parasole.jpg">}}
{{<image_narrow
  src="../images/zamosc_plan.png">}}