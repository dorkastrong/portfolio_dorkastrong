---
title: "Hive 3 Prototype Showcase"
category: "Poster"
image: "blooming_thumb.jpg"
menu: projects
draft: true
---

{{<paragraph>}}For six months in 2019 I have taken part in collaborative residency called [The Hive](https://www.hivers.fr/). Except co-foundation of an eco-feminist collective [Telenatura Lab](www.telenaturalab.com) and experience of sharing the work&life with twenty other creatives, I've designed the poster for the final showcase of the prototypes that we were working on.
All that residency, on top of aboves was for me a pretty special experience and if i wouldn't have proofs I wouldn't be sure if it happened for real.

That's why, if you ask for inspirations for the theme. Plus loud calling of mating frogs, making up every night in the swimming pool.{{</paragraph>}}

{{<image_narrow
  src="../images/blooming_mock.jpg">}}
{{<image_narrow
  src="../images/blooming_4.png">}}
