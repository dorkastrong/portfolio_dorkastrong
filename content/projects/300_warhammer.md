---
title: "Warhammer HR"
category: "Illustration"
image: "warhammer_thumb_2.jpg"
menu: projects
---

{{<paragraph>}}A set of illustrations that I designed for Games Workshop, company behind Warhammer. The illustrations are targeting the HR team members from different departments.{{</paragraph>}} 

{{<image_narrow
  src="../images/warhammer_1.jpg">}}
{{<image_narrow
  src="../images/warhammer_2.jpg">}}
{{<image_narrow
  src="../images/warhammer_4.jpg">}}