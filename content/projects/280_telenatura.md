---
title: "Telenatura"
category: "Visual Identity"
image: "telenatura-thumb-3.jpg"
menu: projects
images:
  - ../images/telenatura-logo-3.jpg
  - ../images/telenatura.jpg
  - ../images/telenatura4.jpg
---

{{<paragraph>}}[Telenatura Lab](https://telenaturalab.com/) is an international eco-feminist collective with artists coming from India, Poland, and the United States. And I'm the one coming from Poland! The foundation of this initiative was a result of 6-month stay at the Hive - a collaborative residency taking place near Marseille, France. Among other activities, I was designing the visual identity of Telenatura Lab, starting from logo, through all the printed materials till exhibition signs.{{</paragraph>}}

{{<image_narrow
  src="../images/telenatura-logo-3.jpg">}}
{{<image_narrow
  src="../images/telenatura4.jpg">}}
{{<image_narrow
  src="../images/telenatura.jpg">}}
