---
title: "Wahlomat"
category: "Infographics"
image: "wahlomat2_thumb.jpg"
menu: projects
draft: true

images:

  - ../images/wahlomat_2.jpg
  
---

The infographics that I've designed for Bundeszentrale für politische Bildung about [Wahlomat](https://www.bpb.de/politik/wahlen/wahl-o-mat/) an app that helps the users/voters to clarify their political position.