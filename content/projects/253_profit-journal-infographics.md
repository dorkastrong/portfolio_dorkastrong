---
title: "Infographics for Profit Journal"
category: "Infographics"
image: "165_profit_thumb.png"
menu: projects
draft: true
---

{{<paragraph>}}For several years I was a creative director and an illustrator of independent economy student magazine - [Profit Journal](http://profitjournal.pl). The main elements of the issues were infographics - which was the biggest fun to make.{{</paragraph>}}

{{<image_narrow
  src="../images/165_profit_info_mock_2.png">}}
{{<image_narrow
  src="../images/165_profit_info.png">}}
