---
title: "Explainer video for Kitodo"
category: "Illustration / Explainer Video"
image: "kitodo_thumb.jpg"
menu: projects
---

{{<paragraph>}}I've created a storyboard, visual style and vector illustrations for explainer video for [Kitodo](https://www.kitodo.org/). Kitodo is a great open source software created in the purpose of digitalisation of cultural resources of archives, libraries, museums and documentation centres. The idea for a style comes from balancing physical structures (paper textures, decorative text elements) with digital ones (pixelisation, glitch), with a reference to origami technique. I was an author of all the visual elements which later got beautifully animated by Berlin based studio [Goldener Westen](https://www.goldenerwesten.net/).{{</paragraph>}} 

{{<video_vimeo id="274707351">}}

{{<image_wide
  src="../images/kitodo_story-01.jpg">}}
{{<image_double
  src-a="../images/kitodo_story-02.jpg"
  src-b="../images/kitodo_story-03.jpg">}}
{{<image_double
  src-a="../images/kitodo_story-05.jpg"
  src-b="../images/kitodo_story-06.jpg">}}
{{<image_double
  src-a="../images/kitodo_story-17.jpg"
  src-b="../images/kitodo_story-08.jpg">}}
{{<image_double
  src-a="../images/kitodo_story-10.jpg"
  src-b="../images/kitodo_story-11.jpg">}}
{{<image_double
  src-a="../images/kitodo_story-14.jpg"
  src-b="../images/kitodo_story-15.jpg">}}