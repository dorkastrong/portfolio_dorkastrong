---
title: "Sauna Stories"
category: "Illustration"
image: "Sauna_thumb.jpg"
menu: projects

---

{{<paragraph>}} My exploration of illustration styles. This story is based on a factual event that happened to me in a Finnish sauna.{{</paragraph>}}

{{<image_narrow
  src="../images/Sauna.jpg">}}


