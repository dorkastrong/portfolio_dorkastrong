---
title: "StarkInnovision"
category: "Logotype"
image: "Starkinnovision-thumb-4.jpg"
menu: projects
---

{{<paragraph>}}
The logotype design for [StarkInnovision](http://starkinnovision.com/en/)-The Agile Coaching for Teams and Organizations.{{</paragraph>}}

{{<image_wide
  src="../images/Starkinnovision-1.jpg">}}
{{<image_wide
  src="../images/Starkinnovision-1-1.jpg">}}
{{<image_double
  src-a="../images/Starkinnovision-2.jpg"
  src-b="../images/Starkinnovision-3.jpg">}}
{{<image_wide
  src="../images/Starkinnovision-icons-2.png">}}
