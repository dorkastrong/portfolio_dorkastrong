---
title: "Seaside"
category: "Illustration"
image: "sea_thumb.jpg"
menu: projects
images:
  - ../images/octopus.jpg
  - ../images/sea_photo.jpg
  - ../images/fishes.jpg
  - ../images/seaside-fish-by-upstruct-muskat-location-02.jpg
  - ../images/seaside-fish-by-upstruct-muskat-bag-03.jpg
  - ../images/seaside-fish-by-upstruct-muskat-brand-02b.jpg
  - ../images/seaside-fish-by-upstruct-muskat-menucard-4-04.jpg
---

{{<paragraph>}}I've got reached out from studio [Upstruct](http://www.upstruct.com/) to create a set of illustrations for the interior design of a new restaurant [Seaside](http://www.seaside-fish.com/). Some of the drawings got incorporated into the menu, some were used as a part of infographics and the octopus got printed in big size on a sail-textile.

Beautiful photos thanks to Upstruct studio.{{</paragraph>}}

{{<image_wide
  src="../images/seaside-fish-by-upstruct-muskat-location-02.jpg">}}
{{<image_wide
  src="../images/seaside-fish-by-upstruct-muskat-brand-02b.jpg">}}
{{<image_double
  src-a="../images/seaside-fish-by-upstruct-muskat-bag-03.jpg"
  src-b="../images/seaside-fish-by-upstruct-muskat-menucard-4-04.jpg">}}
{{<image_narrow
  src="../images/octopus.jpg">}}
{{<image_narrow
  src="../images/fishes.jpg">}}
{{<image_narrow
  src="../images/sea_photo.jpg">}}



