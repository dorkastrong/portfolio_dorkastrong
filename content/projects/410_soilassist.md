---
title: "SoilAssist"
category: "Interactive Infographic"
image: "soil_assist_thumb.png"
menu: projects
draft: true
---

{{<paragraph>}}This interactive infographic was developed for [SoilAssist](https://www.soilassist.de/en/), a part of [Bonares Centre](https://www.bonares.de/). The challenge was to present the effects of soil compaction clearly and engagingly. Together with Johannes Stein we took the concept of comparison infographics to the next level. The user can drag the bar to compare different soil states, dive deeper into specific aspects by clicking one of the points of interest, and in the zoomed-in view, use the bar again to compare the detailed images. {{</paragraph>}}

{{<video_vimeo id="674398311">}}

{{<image_double
  src-a="../images/soil_assist_3.jpg"
  src-b="../images/soil_assist_8.png">}}

{{<paragraph>}}My responsibility lay in the visual aspect of the infographic (illustrations) and UX and UI design. Soon, this infographic will be turned into a widget so that other scientists can use the framework to present their research.{{</paragraph>}}

{{<image_narrow
  src="../images/soil_assist_2.jpg">}}

{{<image_narrow
  src="../images/soil_assist_10.png">}}

{{<paragraph>}}Bonares is a platform that gathers and communicates the research from soil and agriculture-related domains. It provides tools for scientists to conduct the research, collect and store the data and showcase the results.
Since 2020 together with Johannes Stein we are working on redesigning the platform and enhancing the Bonares science communication. The project aims to build a set of tools, elements, and frameworks that scientists can reuse later on.{{</paragraph>}}
