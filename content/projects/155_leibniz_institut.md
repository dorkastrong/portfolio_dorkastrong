---
title: "Infographics for Leibniz Magazine"
category: "Infographics"
image: "leibniz_thumb_2.png"
menu: projects
draft: true
---

{{<paragraph>}}I have designed the infographics to the article about Smart Cities, published in the Leibniz Magazine. The visual guideline for the infographics was developed by studio [Novamondo](https://novamondo.de/), which was supervising the project.{{</paragraph>}}


{{<image_narrow
  src="../images/leibniz.jpg">}}
{{<image_narrow
  src="../images/leibniz_center.png">}}
