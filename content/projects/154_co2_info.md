---
title: "CO2 Emissions"
category: "Infographics"
image: "CO2_info_thumb_2.jpg"
menu: projects
draft: true

images:
  - ../images/CO2_info_mock_2.jpg
  - ../images/CO2_info.png

---

One of the infograohics that I've designed for Profit Journal. The topic is the emission of CO2 in daily life.
