---
title: "Toads Zine"
category: "Illustration / Zine"
image: "toads_thumb3.jpg"
menu: projects
images:
  - ../images/toads_01_01.jpg
  - ../images/toads_01a.jpg
  - ../images/toads_02a.jpg
  - ../images/toads_03a.jpg
  - ../images/toads_04a.jpg
  - ../images/toads_05a.jpg
  - ../images/toads_06a.jpg
  - ../images/toads_08a.jpg
  - ../images/toads_09a.jpg
  - ../images/toads_10a.jpg
  - ../images/toads_11a.jpg
---

{{<paragraph>}}Finally the toad-zine is here! Risoprinted with golden paint in [Penthaus für schöne Formate](http://penthaus.space/). I've spent pretty some fun time digging into the topic, reading articles about toads in the culture and religion in different parts of the world. To clarify: it's a personal project. Why toads? No idea, somehow they are in my head since long time...like a toadstone in amphibious head. But to read more about the toadstone you have to get the zine! (or just check Wikipedia). See you at Zine-fest! {{</paragraph>}}

{{<image_wide
  src="../images/toads_01a.jpg">}}
{{<image_narrow
  src="../images/toads_02a.jpg">}}
{{<image_double
  src-a="../images/toads_03a.jpg"
  src-b="../images/toads_04a.jpg">}}
{{<image_double
  src-a="../images/toads_05a.jpg"
  src-b="../images/toads_06a.jpg">}}
{{<image_double
  src-a="../images/toads_08a.jpg"
  src-b="../images/toads_09a.jpg">}}
{{<image_wide
  src="../images/toads_10a.jpg">}}