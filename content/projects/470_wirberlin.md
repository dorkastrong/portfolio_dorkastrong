---
title: "Unverpackter Leben"
category: "Illustration"
image: "470_wirberlin_thumb.jpg"
menu: projects
---

{{<paragraph>}}I created this illustration for [wirBERLIN](https://wir-berlin.org) - an organization that is committed to sustainable urban development and activation of civic engagement. The challenge was to depict Ngo's fundamental values integrated into a pleasant and peaceful scene.{{</paragraph>}}

{{<image_narrow
  src="../images/470_wirberlin_1.jpg">}}  

