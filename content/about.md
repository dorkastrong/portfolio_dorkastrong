---
title: "About"
menu: main
weight: 1
layout: "about"
contactLabel: "FOR COLLABORATIONS AND COMMISSIONS:"
contactEmailText: "hi@dorkastrong.com"
contactEmailAddress: "hi@dorkastrong.com"
contactEmailSubject: "Hi!"
---

I am a visual designer with ten years of experience in independent practice. My strength lies in illustration and information design. I enjoy working with projects benefiting culture, science, and education. For commissions, I work with projects aligned with my values. I'm currently based in Helsinki and working internationally.

I'm currently working part-time at [Ellery Studio](https://en.ellerystudio.com/).

I am a graduate of {{< link url="https://grafika.asp.krakow.pl/" text="Graphic Design Master Program at Academy of Fine Arts" >}} in Cracow. I am also an alumni of [Fab Academy Barcelona](https://fablabbcn.org/education/postgraduates/fab-academy-barcelona), where I've completed "Fab Academy: How to make almost everything" program - you can check my documentation [here](http://fab.academany.org/2018/labs/barcelona/students/dorota-orlof/)).

**WHAT CAN I DO FOR YOU?**

  * illustration in a wide range of flavours
  * infographic
  * comic-book   
  * style-frame, storyboard, and illustrations for an animated video
  * key visual
  * iconography

**RECOGNITION**

[Polish Graphic Design Awards Honorable Mention 2020](https://www.polishgraphicdesign.com/awards/wyniki-2020)  
[See & Say Portfolio Review Honorable Mention 2020](https://seesay.pl/wyroznieni/?spg_kategoria=instalacjavideoinstalacja&spg_wyroznienie=&spg_edycja=seesay-2020&set_orderby=wyroznienie)
