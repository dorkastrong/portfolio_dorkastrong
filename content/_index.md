---
title: "Dorota Orlof"
image: "../images/dorka.png"
DescriptionA: "Hi, I'm Dora - a visual designer specialised in illustration and information design. Have a look what I do ↓"
DescriptionB:
DescriptionC: "An essential part of my activity is visual exploration in digital and analogue media. Check out what is cooking in the LAB ↓"
---
