---
title: "Telenatura Projecto"
layout: "telenatura"
---

# Telenatura Lab

**Livestream**

Pacific coastal forest  
Costa Rica

**Manifesto**

We share this planet with frogs and birds  
insects and flowers  
Trees and rivers  
Each other  
But sometimes we forget  
We are trapped in our own lives  
Limited by our perspective  
We at Telenatura Lab believe  
If everyone can take a moment  
And listen to the voices of our planetary cohabitants  
We will remember  
That we are part of something bigger  
A huge system  
Where every being plays a role  
But the voices of these beings  
The trees and frogs  
Aren’t always heard  
Sometimes they are forgotten  
But we have to think of them  
When we make choices about the future  
So we can build a better world  
Where everyone is included  

**Take action**

Link to NGO  
Green city participatory actions