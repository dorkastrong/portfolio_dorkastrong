# Doras Portfolio

This is a repository for Dorota's Hugo website.

## Notes for Dora

> Please remember to run `hugo` locally before doing `git push`. 

The actual website is going to be re-generated in the `public` folder. If in trouble, you can upload all the files you can find in the `public` folder to a webserver of your choice. Any webserver works since the website is plain HTML.

Test.
