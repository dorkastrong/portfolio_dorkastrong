---
title: "20 Jahre Krakauer Haus in Nuerenberg"
category: "Book design / illustration"
image: "norka_thumb.jpg"
menu: projects
images:
  - ../images/norka_info.jpg
  - ../images/norka_x_2.jpg
  - ../images/norka_x_3.jpg
  - ../images/norka_x_4.jpg
  - ../images/norka_2.jpg
  - ../images/norka_1.jpg
  - ../images/norka_4.jpg
  - ../images/norka_3.jpg
  - ../images/norka_8.jpg
  - ../images/norka_5.jpg
  - ../images/norka_6.jpg
  - ../images/norka_7.jpg
---

The visual identity for the celebration of 20 years anniversary of cooperation between Krakauer Haus in Nurenberg and Nuernbergers House in Kraków. The biggest part of the project was the design of publication describing the history of cooperation. In the middle of the book one can find a 4-page-centrefold showing in playful way the history line.
Additionally I've designed the flyers, posters and exhibition signs.
