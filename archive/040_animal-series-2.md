---
title: "Animal series 2"
category: "Illustration / personal project"
image: "atlas_thumb.jpg"
menu: projects
images:
  - ../images/atlas_1.jpg
  - ../images/atlas_2.jpg
  - ../images/atlas_3.jpg
  - ../images/atlas_4.jpg
  - ../images/atlas_5.jpg
  - ../images/atlas_6.jpg
  - ../images/atlas_7.jpg
  - ../images/atlas_8.jpg
---

Atlas of Animals is my graduation project, It's a metapherical encyclopedia picturing the connections and references to animal presence in the present-day. Proverbs, religious connotations, culture icons. Each illustration is asking the recipient for the interpretation. Enjoy!
