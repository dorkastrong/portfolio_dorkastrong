$(document).ready( function(){
  var fitvids = function(){
    $(".video-youtube iframe").each( function(){
      var ar = $(this).attr("height") / $(this).attr("width");
      var w = $(this).width();
      var h = w * ar;
      console.log(w, h);
      $(this).css("height", h + "px");
    });
  }

  // fit iframe videos once page loads
  fitvids();

  // fit iframe videos whenever browser window is resized
  $(window).resize( function(ev){
    fitvids();
  });
});
