(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"manarola_fire_atlas_", frames: [[2669,0,113,133],[2669,135,113,133],[0,0,2667,1771]]}
];


// symbols:



(lib.Mesh = function() {
	this.initialize(ss["manarola_fire_atlas_"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Mesh_1 = function() {
	this.initialize(ss["manarola_fire_atlas_"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.manarola_bg01 = function() {
	this.initialize(ss["manarola_fire_atlas_"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFC21F","#FF4800","rgba(255,72,0,0)"],[0.094,0.333,0.973],0,0.1,0,0,0.1,80.4).s().p("AoxIyQjojpAAlJQAAlIDojpQDpjoFIgBQFJABDoDoQDpDpAAFIQAAFJjpDpQjoDplJAAQlIAAjpjpg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-79.4,-79.4,158.9,158.9);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#E9FFAE","#F7873C","rgba(255,72,0,0)"],[0.094,0.239,0.51],0,0,0,0,0,129.8).s().p("AuPOQQl5l6AAoWQAAoUF5l6QF6l6IVAAQIWAAF5F6QF6F6AAIUQAAIWl6F6Ql5F5oWAAQoVAAl6l5g");
	this.shape.setTransform(0,1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-128.8,-127.8,257.70000000000005,257.7);


(lib.tail_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8,1,1).p("AAArnQA+BGAeB4QA9DwiZD5QiYD4A9EwQAfCZA8Bn");
	this.shape.setTransform(40.85,0.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8,1,1).p("AAALpQg4hlgdiYQg4kwCNj7QCOj5g5jxQgch4g5hH");
	this.shape_1.setTransform(40.9061,0.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8,1,1).p("AAALpQg0hjgaiXQgzkuCBj/QCDj6g0jxQgah5g1hG");
	this.shape_2.setTransform(40.9751,0.125);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8,1,1).p("AgBLqQguhigYiWQgvktB1kCQB5j6gwjyQgYh5gwhH");
	this.shape_3.setTransform(41.0313,0.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8,1,1).p("AgCLrQgqhhgViVQgqkrBpkGQBuj7grjzQgWh5grhH");
	this.shape_4.setTransform(41.1003,-0.075);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AgCLsQglhfgTiVQglkpBdkKQBkj7gnj0QgUh6gnhH");
	this.shape_5.setTransform(41.1667,-0.175);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8,1,1).p("AgDLtQgghegRiUQggkoBRkOQBaj7gjj0QgRh7gjhH");
	this.shape_6.setTransform(41.223,-0.25);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8,1,1).p("AgDLuQgchcgOiUQgbknBFkRQBPj7gfj2QgPh6gehI");
	this.shape_7.setTransform(41.2791,-0.35);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8,1,1).p("AgELvQgWhbgMiTQgXklA5kVQBFj8gaj2QgNh7gahI");
	this.shape_8.setTransform(41.3355,-0.45);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8,1,1).p("AgFLwQgShagJiSQgSkkAtkYQA6j8gVj3QgLh7gWhJ");
	this.shape_9.setTransform(41.405,-0.525);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8,1,1).p("AgFLxQgNhYgHiSQgNkiAhkcQAvj9gRj3QgIh8gShJ");
	this.shape_10.setTransform(41.4619,-0.625);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8,1,1).p("AgGLyQgIhXgFiRQgIkgAVkgQAlj9gMj4QgGh9gOhJ");
	this.shape_11.setTransform(41.5194,-0.725);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(8,1,1).p("AgGLzQgEhWgCiPQgDkgAJkjQAaj+gIj5QgEh9gJhJ");
	this.shape_12.setTransform(41.5911,-0.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(8,1,1).p("AgGL0QABhUAAiPQACkegDknQAQj/gEj5QgCh9gEhK");
	this.shape_13.setTransform(41.5549,-0.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(8,1,1).p("AgEL1QAFhTADiOQAFkdgNkqQAGj/AAj7QABh9gChK");
	this.shape_14.setTransform(41.3756,-1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(8,1,1).p("AgIL2QAKhSAFiNQALkcgaktQgDkAAFj7QACh9ADhL");
	this.shape_15.setTransform(41.7269,-1.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(8,1,1).p("AgJL2QAPhPAIiNQAPkagmkxQgOkAAKj8QAEh+AIhK");
	this.shape_16.setTransform(41.8183,-1.175);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(8,1,1).p("AgJL3QAThOAKiMQAUkYgxk1QgZkBAOj8QAHh/AMhK");
	this.shape_17.setTransform(41.8791,-1.275);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(8,1,1).p("AgKL4QAZhNAMiLQAZkXg+k4QgjkBATj9QAJh/AQhL");
	this.shape_18.setTransform(41.9373,-1.375);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(8,1,1).p("AgLL5QAdhMAPiKQAekWhKk8QgtkBAXj+QALh/AUhL");
	this.shape_19.setTransform(42.0073,-1.45);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(8,1,1).p("AgLL6QAihKARiKQAikUhVlAQg4kCAbj+QAOiAAYhL");
	this.shape_20.setTransform(42.064,-1.55);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(8,1,1).p("AgML7QAnhIAUiKQAnkShilEQhCkCAfkAQAQh/AdhM");
	this.shape_21.setTransform(42.1218,-1.65);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(8,1,1).p("AgML8QArhHAWiJQAskRhtlHQhNkCAkkBQASiAAhhM");
	this.shape_22.setTransform(42.1781,-1.725);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(8,1,1).p("AgNL9QAxhGAYiIQAxkQh6lKQhXkDAokBQAViAAlhN");
	this.shape_23.setTransform(42.2451,-1.825);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(8,1,1).p("AgOL+QA1hFAbiHQA2kOiGlOQhhkDAskCQAXiBAphN");
	this.shape_24.setTransform(42.314,-1.925);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(8,1,1).p("AgOL/QA6hDAdiHQA6kMiRlSQhskEAxkCQAYiCAuhN");
	this.shape_25.setTransform(42.37,-2.025);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(8,1,1).p("AgPMAQA/hCAgiFQA/kMielVQh2kFA1kDQAbiCAyhN");
	this.shape_26.setTransform(42.4389,-2.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(8,1,1).p("AgDsAQg2BOgdCCQg6EECBEFQCpFZhEEKQgiCEhDBB");
	this.shape_27.setTransform(42.4949,-2.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AimClQhFhHAAheQAAhdBFhHQBGhHBgAAQBgAABGBHQBGBHAABdQAABehGBHQhGBHhgAAQhgAAhGhHg");
	this.shape.setTransform(23.625,23.575);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_2_Layer_1, null, null);


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AlXqaIKuAAIj6U1g");
	this.shape.setTransform(61.7555,20.8828,0.4686,0.3054,14.9973);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AlXqaIKuAAIj6U1g");
	this.shape_1.setTransform(15.7994,22.3039,0.4595,0.3054);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_1_Layer_1, null, null);


(lib.spark_line = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AlkCjQgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAWQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_1 = new cjs.Graphics().p("AlpB/QgQgPAAgWQAAgWAQgQQAQgQAWAAQAWAAAPAQQAQAQAAAWQAAAWgQAPQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_2 = new cjs.Graphics().p("AlpBcQgPgQAAgWQAAgWAPgQQAQgPAWAAQAWAAAQAPQAQAQAAAWQAAAWgQAQQgQAQgWAAQgWAAgQgQg");
	var mask_graphics_3 = new cjs.Graphics().p("AlhA5QgPgPAAgWQAAgVAPgQQAQgQAWAAQAWAAAQAQQAQAQAAAVQAAAWgQAPQgQAQgWAAQgWAAgQgQg");
	var mask_graphics_4 = new cjs.Graphics().p("AlOAmQgQgQAAgWQAAgVAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAVQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_5 = new cjs.Graphics().p("AkyAmQgQgQAAgWQAAgVAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAVQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_6 = new cjs.Graphics().p("AkaAlQgQgPAAgWQAAgVAQgQQAQgQAWAAQAWAAAPAQQAQAQAAAVQAAAWgQAPQgPARgWgBQgWABgQgRg");
	var mask_graphics_7 = new cjs.Graphics().p("AkAAlQgQgPAAgWQAAgVAQgQQAQgQAWAAQAWAAAPAQQAQAQAAAVQAAAWgQAPQgPAQgWABQgWgBgQgQg");
	var mask_graphics_8 = new cjs.Graphics().p("AjlAmQgPgQAAgWQAAgVAPgQQAQgPAWAAQAWAAAQAPQAQAQAAAVQAAAWgQAQQgQAQgWAAQgWAAgQgQg");
	var mask_graphics_9 = new cjs.Graphics().p("AjKAlQgPgPAAgWQAAgVAPgQQAQgQAWAAQAWAAAQAQQAQAQAAAVQAAAWgQAPQgQAQgWABQgWgBgQgQg");
	var mask_graphics_10 = new cjs.Graphics().p("AivAlQgPgPAAgWQAAgVAPgQQAQgQAWAAQAWAAAQAQQAQAQAAAVQAAAWgQAPQgQAQgWABQgWgBgQgQg");
	var mask_graphics_11 = new cjs.Graphics().p("AiVAlQgQgPAAgWQAAgVAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAVQAAAWgQAPQgPAQgWABQgWgBgQgQg");
	var mask_graphics_12 = new cjs.Graphics().p("Ah+AmQgPgQAAgWQAAgVAPgQQAQgPAWAAQAWAAAQAPQAQAQAAAVQAAAWgQAQQgQAPgWAAQgWAAgQgPg");
	var mask_graphics_13 = new cjs.Graphics().p("AhmAmQgQgQAAgWQAAgVAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAVQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_14 = new cjs.Graphics().p("AhPAmQgPgQAAgWQAAgVAPgQQAQgPAWAAQAWAAAQAPQAPAQAAAVQAAAWgPAQQgQAPgWAAQgWAAgQgPg");
	var mask_graphics_15 = new cjs.Graphics().p("Ag2AlQgQgPAAgWQAAgVAQgQQAQgQAWAAQAVAAAPAQQAQAQAAAVQAAAWgQAPQgPARgVgBQgWABgQgRg");
	var mask_graphics_16 = new cjs.Graphics().p("AglAmQgPgQAAgWQAAgVAPgQQAQgQAVAAQAWAAAQAQQAQAQAAAVQAAAWgQAQQgQAPgWAAQgVAAgQgPg");
	var mask_graphics_17 = new cjs.Graphics().p("AglAcQgPgPAAgVQAAgWAPgQQAQgQAVAAQAWAAAQAQQAPAQAAAWQAAAVgPAPQgQAQgWAAQgVAAgQgQg");
	var mask_graphics_18 = new cjs.Graphics().p("AglAUQgPgQAAgVQAAgWAPgQQAQgPAVAAQAWAAAPAPQAQAQABAWQgBAVgQAQQgPAQgWAAQgVAAgQgQg");
	var mask_graphics_19 = new cjs.Graphics().p("AgfAIQgQgOAAgWQAAgWAQgQQAQgQAVAAQAWAAAPAQQAQAQAAAWQAAAWgQAOQgPAQgWAAQgVAAgQgQg");
	var mask_graphics_20 = new cjs.Graphics().p("AgWgHQgQgPAAgWQAAgWAQgQQAQgQAVAAQAWAAAPAQQAQAQAAAWQAAAWgQAPQgPAPgWAAQgVAAgQgPg");
	var mask_graphics_21 = new cjs.Graphics().p("AgTgZQgPgQAAgWQAAgWAPgQQAQgPAVAAQAWAAAQAPQAQAQAAAWQAAAWgQAQQgQAQgWAAQgVAAgQgQg");
	var mask_graphics_22 = new cjs.Graphics().p("AgTgsQgPgQAAgWQAAgWAPgQQAQgPAVAAQAWAAAQAPQAQAQAAAWQAAAWgQAQQgQAQgWAAQgVAAgQgQg");
	var mask_graphics_23 = new cjs.Graphics().p("AgVg/QgPgQAAgWQAAgWAPgQQAQgPAVAAQAWAAAQAPQAQAQAAAWQAAAWgQAQQgQAQgWAAQgVAAgQgQg");
	var mask_graphics_24 = new cjs.Graphics().p("AgYhSQgPgPAAgWQAAgWAPgQQAQgQAVAAQAWAAAQAQQAQAQAAAWQAAAWgQAPQgQAQgWAAQgVAAgQgQg");
	var mask_graphics_25 = new cjs.Graphics().p("AgfhoQgPgPAAgWQAAgWAPgQQAQgQAVAAQAWAAAQAQQAQAQAAAWQAAAWgQAPQgQAQgWAAQgVAAgQgQg");
	var mask_graphics_26 = new cjs.Graphics().p("Aglh9QgQgPAAgWQAAgWAQgQQAQgQAVAAQAWAAAQAQQAPAQAAAWQAAAWgPAPQgQAQgWAAQgVAAgQgQg");
	var mask_graphics_27 = new cjs.Graphics().p("AgliQQgPgQAAgWQAAgWAPgQQAQgPAVAAQAWAAAPAPQAQAQABAWQgBAWgQAQQgPAQgWAAQgVAAgQgQg");
	var mask_graphics_28 = new cjs.Graphics().p("AgliiQgQgPAAgWQAAgWAQgQQAQgQAVAAQAWAAAPAQQARAQgBAWQABAWgRAPQgPAQgWAAQgVAAgQgQg");
	var mask_graphics_29 = new cjs.Graphics().p("AglixQgQgQAAgWQAAgWAQgQQAQgPAVAAQAWAAAPAPQAQAQAAAWQAAAWgQAQQgPAQgWAAQgVAAgQgQg");
	var mask_graphics_30 = new cjs.Graphics().p("Ag1i7QgPgPAAgWQAAgWAPgQQAQgQAWAAQAVAAAQAQQAQAQAAAWQAAAWgQAPQgQAQgVAAQgWAAgQgQg");
	var mask_graphics_31 = new cjs.Graphics().p("Ag/i/QgPgPAAgWQAAgWAPgQQAQgQAWAAQAWAAAPAQQAQAQAAAWQAAAWgQAPQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_32 = new cjs.Graphics().p("AhIjBQgQgPAAgWQAAgWAQgQQAQgQAWAAQAWAAAOAQQAQAQAAAWQAAAWgQAPQgOAQgWAAQgWAAgQgQg");
	var mask_graphics_33 = new cjs.Graphics().p("AhTjAQgPgPAAgWQAAgWAPgQQAQgQAWAAQAWAAAQAQQAPAQAAAWQAAAWgPAPQgQAQgWAAQgWAAgQgQg");
	var mask_graphics_34 = new cjs.Graphics().p("Ahbi6QgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAWQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_35 = new cjs.Graphics().p("AhhixQgPgQAAgWQAAgWAPgQQAQgPAWAAQAWAAAQAPQAQAQAAAWQAAAWgQAQQgQAQgWAAQgWAAgQgQg");
	var mask_graphics_36 = new cjs.Graphics().p("AhkinQgQgPAAgWQAAgWAQgQQAQgQAWAAQAWAAAPAQQAQAQAAAWQAAAWgQAPQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_37 = new cjs.Graphics().p("AhkicQgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAWQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_38 = new cjs.Graphics().p("AhjiSQgPgQAAgWQAAgWAPgQQAQgPAWAAQAWAAAQAPQAQAQAAAWQAAAWgQAQQgQAQgWAAQgWAAgQgQg");
	var mask_graphics_39 = new cjs.Graphics().p("AhfiIQgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAWQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_40 = new cjs.Graphics().p("Ahbh/QgQgPAAgWQAAgWAQgQQAQgQAWAAQAWAAAPAQQAQAQAAAWQAAAWgQAPQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_41 = new cjs.Graphics().p("AhVh1QgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAPAPQAPAQAAAWQAAAWgPAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_42 = new cjs.Graphics().p("AhKhwQgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAOAPQAQAQAAAWQAAAWgQAQQgOAQgWAAQgWAAgQgQg");
	var mask_graphics_43 = new cjs.Graphics().p("Ag/hyQgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAOAPQAQAQAAAWQAAAWgQAQQgOAQgWAAQgWAAgQgQg");
	var mask_graphics_44 = new cjs.Graphics().p("Ag2h5QgPgPAAgWQAAgWAPgQQAQgQAWAAQAVAAAQAQQAQAQAAAWQAAAWgQAPQgQAQgVAAQgWAAgQgQg");
	var mask_graphics_45 = new cjs.Graphics().p("AgxiDQgQgPAAgWQAAgWAQgQQAQgQAWAAQAVAAAPAQQAQAQAAAWQAAAWgQAPQgPAQgVAAQgWAAgQgQg");
	var mask_graphics_46 = new cjs.Graphics().p("Ag8iTQgQgPAAgWQAAgWAQgQQAQgQAWAAQAWAAAOAQQAQAQAAAWQAAAWgQAPQgOAQgWAAQgWAAgQgQg");
	var mask_graphics_47 = new cjs.Graphics().p("AhNieQgPgPAAgWQAAgWAPgQQAQgQAWAAQAWAAAQAQQAPAQAAAWQAAAWgPAPQgQAQgWAAQgWAAgQgQg");
	var mask_graphics_48 = new cjs.Graphics().p("AhhijQgQgPAAgWQAAgWAQgQQAQgQAWAAQAWAAAPAQQAQAQAAAWQAAAWgQAPQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_49 = new cjs.Graphics().p("Ah2ifQgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAWQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_50 = new cjs.Graphics().p("AiOiSQgPgPAAgWQAAgWAPgQQAQgQAWAAQAWAAAQAQQAQAQAAAWQAAAWgQAPQgQAQgWAAQgWAAgQgQg");
	var mask_graphics_51 = new cjs.Graphics().p("Aijh1QgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAWQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_52 = new cjs.Graphics().p("AimhSQgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAWQAAAWgQAQQgPAQgWAAQgWAAgQgQg");
	var mask_graphics_53 = new cjs.Graphics().p("AiagwQgPgQAAgWQAAgWAPgQQAQgPAWAAQAWAAAQAPQAQAQAAAWQAAAWgQAQQgQAQgWAAQgWAAgQgQg");
	var mask_graphics_54 = new cjs.Graphics().p("AiEgSQgQgQAAgWQAAgWAQgQQAQgPAWAAQAWAAAPAPQAQAQAAAWQAAAWgQAQQgPAQgWAAQgWAAgQgQg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-37.275,y:17.875}).wait(1).to({graphics:mask_graphics_1,x:-37.783,y:14.3441}).wait(1).to({graphics:mask_graphics_2,x:-37.7313,y:10.7748}).wait(1).to({graphics:mask_graphics_3,x:-36.9278,y:7.3142}).wait(1).to({graphics:mask_graphics_4,x:-35.089,y:3.35}).wait(1).to({graphics:mask_graphics_5,x:-32.2665,y:-0.4722}).wait(1).to({graphics:mask_graphics_6,x:-29.8944,y:-2.4311}).wait(1).to({graphics:mask_graphics_7,x:-27.2632,y:-3.6157}).wait(1).to({graphics:mask_graphics_8,x:-24.5168,y:-4.0489}).wait(1).to({graphics:mask_graphics_9,x:-21.8161,y:-3.9433}).wait(1).to({graphics:mask_graphics_10,x:-19.1285,y:-3.4645}).wait(1).to({graphics:mask_graphics_11,x:-16.5772,y:-2.7599}).wait(1).to({graphics:mask_graphics_12,x:-14.2483,y:-2.205}).wait(1).to({graphics:mask_graphics_13,x:-11.8897,y:-1.9321}).wait(1).to({graphics:mask_graphics_14,x:-9.5022,y:-2.1501}).wait(1).to({graphics:mask_graphics_15,x:-7.0781,y:-3.2519}).wait(1).to({graphics:mask_graphics_16,x:-4.3513,y:-5.2129}).wait(1).to({graphics:mask_graphics_17,x:-0.0297,y:-6.2967}).wait(1).to({graphics:mask_graphics_18,x:3.676,y:-7.1104}).wait(1).to({graphics:mask_graphics_19,x:5.9145,y:-8.2744}).wait(1).to({graphics:mask_graphics_20,x:6.8123,y:-9.8684}).wait(1).to({graphics:mask_graphics_21,x:7.1833,y:-11.7101}).wait(1).to({graphics:mask_graphics_22,x:7.1798,y:-13.6163}).wait(1).to({graphics:mask_graphics_23,x:6.9716,y:-15.5281}).wait(1).to({graphics:mask_graphics_24,x:6.6761,y:-17.3928}).wait(1).to({graphics:mask_graphics_25,x:5.9576,y:-19.5717}).wait(1).to({graphics:mask_graphics_26,x:4.4441,y:-21.6645}).wait(1).to({graphics:mask_graphics_27,x:1.8111,y:-23.6171}).wait(1).to({graphics:mask_graphics_28,x:-1.3008,y:-25.3904}).wait(1).to({graphics:mask_graphics_29,x:-4.9449,y:-26.9101}).wait(1).to({graphics:mask_graphics_30,x:-6.9175,y:-27.8734}).wait(1).to({graphics:mask_graphics_31,x:-7.9445,y:-28.2517}).wait(1).to({graphics:mask_graphics_32,x:-8.8892,y:-28.4546}).wait(1).to({graphics:mask_graphics_33,x:-9.9415,y:-28.3886}).wait(1).to({graphics:mask_graphics_34,x:-10.7843,y:-27.845}).wait(1).to({graphics:mask_graphics_35,x:-11.3201,y:-26.9408}).wait(1).to({graphics:mask_graphics_36,x:-11.676,y:-25.8659}).wait(1).to({graphics:mask_graphics_37,x:-11.6832,y:-24.841}).wait(1).to({graphics:mask_graphics_38,x:-11.502,y:-23.8136}).wait(1).to({graphics:mask_graphics_39,x:-11.1871,y:-22.8116}).wait(1).to({graphics:mask_graphics_40,x:-10.7527,y:-21.852}).wait(1).to({graphics:mask_graphics_41,x:-10.1509,y:-20.9199}).wait(1).to({graphics:mask_graphics_42,x:-9.097,y:-20.4234}).wait(1).to({graphics:mask_graphics_43,x:-7.9908,y:-20.6443}).wait(1).to({graphics:mask_graphics_44,x:-7.0413,y:-21.2971}).wait(1).to({graphics:mask_graphics_45,x:-6.5865,y:-22.2897}).wait(1).to({graphics:mask_graphics_46,x:-7.6692,y:-23.882}).wait(1).to({graphics:mask_graphics_47,x:-9.3334,y:-24.985}).wait(1).to({graphics:mask_graphics_48,x:-11.3721,y:-25.4882}).wait(1).to({graphics:mask_graphics_49,x:-13.4961,y:-25.1488}).wait(1).to({graphics:mask_graphics_50,x:-15.8294,y:-23.7589}).wait(1).to({graphics:mask_graphics_51,x:-17.9799,y:-20.9266}).wait(1).to({graphics:mask_graphics_52,x:-18.2943,y:-17.4344}).wait(1).to({graphics:mask_graphics_53,x:-17.0372,y:-14.031}).wait(1).to({graphics:mask_graphics_54,x:-14.875,y:-11.025}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFBD").s().p("AmBGfQgCgDAFgDQAFgDABAEQACAEgEACIgDABQgBAAAAAAQgBAAAAAAQgBgBAAAAQAAAAgBgBgAmLF9QgEgHAIgGQAHgGAFAJQAFAJgJAEIgFABQgFAAgCgEgAmOFbQgCgEAFgEQAGgDADAGQACAFgFACIgEABQgDAAgCgDgAmRFCQgCgHAEgDQAEgDAFAGQAEAGgHAEIgEABQgDAAgBgEgAmSEmQgEgGAHgEQAIgEADAGQACAGgGAEIgEABQgEAAgCgDgAmPEHQgDgFAGgFQAFgEAEAHQAEAHgHADIgEABQgDAAgCgEgAmODsQgFgHAKgGQAJgFAEAJQAFAJgJAEIgGABQgFAAgDgFgAmDDOQgCgIAFgEQAGgEAFAIQAFAHgIAFIgGABQgDAAgCgFgAl4C2QgEgFAJgFQAIgFADAHQADAHgHAEIgFABQgEAAgDgEgAloCYQgEgIAJgHQAIgGAGAKQAGAKgKAFIgHABQgFAAgDgFgAlOCEQgDgFAGgDQAGgEADAGQADAGgGACIgDABQgEAAgCgDgAk6B1QgCgHAFgDQAEgDAEAGQAFAGgHAEIgFABQgDAAgBgEgAkcBlQgEgFAIgFQAIgEACAHQADAGgHADIgEABQgDAAgDgDgAj9BdQgEgIAIgGQAIgGAGAJQAFAKgKAEIgGACQgEAAgDgFgAjiBZQgEgGAHgEQAIgFAEAIQADAHgHADIgFABQgEAAgCgEgAjABYQgBgFADgDQAEgCADAFQAEAEgGADIgDABQgBAAAAAAQgBAAAAAAQgBgBAAAAQAAgBgBgBgACeBUQgCgHAEgDQAFgDAFAGQAFAGgIAFIgFABQgDAAgBgFgAAtBSQgCgIAFgDQAFgDAFAGQAFAHgIAEIgFACQgDAAgCgFgACDBRQgEgGAHgEQAHgEAEAHQADAHgHACIgEABQgEAAgCgDgABiBQQgDgGAGgEQAGgFAEAIQAEAHgHADIgEABQgEAAgCgEgABLBPQgFgHAKgFQAJgFADAIQADAHgHAEIgGACQgEAAgDgEgAgQBOQgDgHAHgGQAHgFAFAIQADAJgHAEIgFABQgEAAgDgEgAC8BNQgEgGAIgEQAJgFADAHQADAHgHAEIgFABQgEAAgDgEgAiiBOQgEgFAIgEQAHgEACAGQADAGgGADIgEABQgEAAgCgDgAAPBNQgEgFAHgEQAHgEAEAGQADAHgHADIgEABQgEAAgCgEgAhrBLQgEgHAJgEQAIgFAEAIQAEAIgIADIgGABQgEAAgDgEgAhKBLQgBgFADgCQADgCADAEQAEAFgGACIgDABQAAAAgBAAQAAAAgBAAQAAgBAAAAQgBgBAAgBgAiDBLQgCgDAEgDQAEgDADAEQADAFgFACIgDABQAAAAgBAAQgBAAAAgBQgBAAAAgBQAAAAgBgBgAgxBJQgEgGAJgFQAIgEADAHQADAHgHADIgFABQgEAAgDgDgADaBJQgDgFAFgEQAFgEAEAGQADAGgGADIgEABQgDAAgBgDgAD7BDQgEgGAHgEQAIgFAEAIQADAHgHADIgFABQgEAAgCgEgAETAzQgCgHAFgEQAFgDAEAGQAFAHgIAEIgEACQgEAAgBgFgAEuAoQgGgIALgGQAMgGAEAJQADAJgJAFQgEABgDAAQgEAAgEgEgAFEAXQgDgHAHgGQAHgFAFAIQAEAJgIAEIgFABQgEAAgDgEgAFgAEQgDgFAGgEQAHgDADAGQADAGgGACIgEABQgEAAgCgDgAFsgYQgCgHAFgDQAEgEAFAHQAEAGgHAEIgFABQgDAAgBgEgABjgxQgEgHAGgDQAGgDAEAGQAEAHgGADIgDABQgEAAgDgEgABDg9QgFgHAIgEQAHgEAFAHQAFAIgIAEIgFABQgEAAgDgFgAF7g7QgDgFAGgEQAHgDACAFQADAGgGACIgEABQgDAAgCgCgAGChNQgDgHAHgGQAHgFAFAIQAEAJgIAEIgFABQgEAAgDgEgAAuhPQgDgGAGgDQAGgDAFAFQAFAFgHAEQgDACgDAAQgEAAgCgEgAAYhoQgHgHAJgGQAJgFAEAIQAEAJgGAEIgFABQgEAAgEgEgAGHhyQgDgEAGgEQAGgDADAFQADAGgGADIgDABQgEAAgCgEgAAKiCQgEgGAGgCQAFgDAEAGQAEAGgGACIgDABQgDAAgDgEgAGIiSQgBgHAEgCQAEgDAEAFQAEAGgGADIgFACQgCAAgCgEgAADiaQgEgHAHgEQAHgEAFAHQAFAIgIAEIgFABQgEAAgDgFgAGDirQgEgFAIgFQAIgEADAGQADAHgHADIgEACQgEAAgDgEgAABi9QgDgHAGgDQAHgDAFAFQAFAGgIAEIgGACQgEAAgCgEgAF2jKQgDgFAFgEQAFgEAEAGQADAGgGADIgEABQgDAAgBgDgAAEjgQgFgGAHgFQAIgEADAHQAEAHgGADIgEACQgDAAgEgEgAFtjnQgDgFAGgEQAGgDADAGQADAGgGACIgEABQgDAAgCgDgAAMj7QgFgHAHgDQAGgEAFAIQAEAHgGADIgEABQgEAAgDgFgAFej8QgDgKAGgEQAGgEAGAIQAGAIgJAGQgEACgCAAQgFAAgBgGgAC0kGQgFgIAJgEQAIgEAGAGQAGAHgKAFQgEADgDAAQgEAAgDgFgADXkIQgFgHAIgEQAHgEAFAHQAFAIgIAEIgFABQgEAAgDgFgAAekNQgFgHAIgEQAHgEAFAHQAFAIgIAEIgFABQgEAAgDgFgACfkeQgFgEAGgEQAHgEACAGQADAGgEADIgDABQgDAAgDgEgAFVkfQgCgDAFgDQAFgDABAEQACAEgEACIgDABQgBAAAAAAQgBAAAAAAQgBgBAAAAQAAAAgBgBgAArkoQgFgIAJgEQAIgEAGAHQAGAGgKAGQgEACgDAAQgEAAgDgFgADakqQgEgHAGgDQAHgEAEAIQAFAHgHADIgDABQgFAAgDgFgAE9ktQgDgGAGgFQAHgGAFAJQAEAIgIADIgFABQgEAAgCgEgACIkyQgEgGAGgDQAFgCADAFQAEAGgFADIgDABQgDAAgDgEgADGlCQgGgGAHgEQAIgFAEAHQADAIgFADIgEABQgDAAgEgEgABGlDQgEgEAGgEQAGgEADAGQADAGgEADIgDABQgDAAgEgEgACNlHIgDACQgLAFgGgKQgHgKAKgGQALgFAGAKIACACIAAAAQAIgEAFAHQAFAIgIAEIgFABQgEAAgDgEgACplIQgDgGAGgDQAHgDAFAFQAFAFgIAFIgFACQgEAAgDgFgABelLQgEgGAGgDQAFgCADAFQAEAGgFADIgDABQgDAAgDgEgAEwlOQgDgEAGgDQAFgDACAFQADAFgFACIgEABQgDAAgBgDgAEblaQgCgKAGgEQAGgFAGAJQAGAIgKAGQgDACgDAAQgEAAgCgGgAB+lgQgDgFAHgEQAHgEADAGQADAHgHADIgEABQgDAAgDgEgAEHlyQgEgFAHgFQAIgEADAHQACAGgGADIgEABQgEAAgCgDgACEl+QgDgFAFgFQAGgEAEAHQADAHgGACIgEABQgEAAgBgDgADvmBQgDgHAHgFQAGgEAFAHQAEAIgIADIgFACQgEAAgCgEgACcmSQgDgFAHgDQAGgEACAFQACAGgFACIgEABQgDAAgCgCgADUmTQgDgEAGgDQAFgDACAFQADAFgFACIgEABQgDAAgBgDgAC7mXQgCgGAEgCQADgDAEAFQADAFgFADIgEABQAAAAgBAAQAAAAgBgBQAAAAgBgBQAAAAAAgBg");
	this.shape.setTransform(-30.9275,-10.4267);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(55));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-71.4,-52.1,81,83.4);


(lib.single_spark = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFBD").s().p("AhtBuQgtguAAhAQAAg/AtgtQAuguA/AAQBAAAAtAuQAuAtAAA/QAABAguAuQgtAthAAAQg/AAgugtg");
	this.shape.setTransform(-0.25,-0.45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.7,-15.9,31,31);


(lib.Scene_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.manarola_bg01();
	this.instance.parent = this;
	this.instance.setTransform(-4,-0.55,0.4836,0.4836);

	this.instance_1 = new lib.manarola_bg01();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,0.4799,0.4799);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(109));

}).prototype = p = new cjs.MovieClip();


(lib.mouth_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC99").s().p("A80VLMAAAgqVMA5pAAAMAAAAqVg");
	this.shape.setTransform(-48.475,-13.45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mouth_Layer_1, null, null);


(lib.head_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AhBDVQgPgCgLgLQgLgKgEgPQgYAAgRgSQgRgRAAgZQAAgdgVgPIgzglQgRgMgDgUQgEgUAMgQIAZgiQAPh9B6gTICnAAQBTgCAsArQAmAkAMBJIgzCHIAAAEQAAAZgPATQgcAiguAQQhIAYgQAJQgSAKgTAAIg6gBg");
	this.shape.setTransform(0.0105,-0.0026);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.head_Layer_1, null, null);


(lib.flame = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFBD").s().p("EgE2EHXQwDwzt6zqQzd7gsZ9TUgPegklgDeglTUgDeglSAMohCqUAKHg1VAT5hFoUAOOgxvARrg04QI26dF/wgQFQPoH6ZKUAP0AyUANSAvqUASmBCuAK4Az4UANmBA1AAsAl0UAAsAl0gMBAmqQpne9xQeEQsVVeu7S6QneJdk/FKQlQkeoAoag");
	this.shape.setTransform(531.2016,6.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.4,-1761.5,1105.2,3535.8);


(lib.ear_right_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AiWBsQgFgYANgdIANgYIB+i3QASgcAhAAQAhgBAUAaIAXAfQASAXAGAcQAHAdgFAdIgeCaQgDATgOALQgPAMgSAAIgXAAIgNAAQipAAgPhJg");
	this.shape.setTransform(0.0246,0.0288);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ear_right_Layer_1, null, null);


(lib.ear_left_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgWC2IgTAAQgLAAgJgGQgJgHgDgKIglhtQgFgNgLgHQgSgMgCgSIgPiNQgDgdAagNQAagNAWAUIDYDKQBNBbhUAtQgaAOgnAIIgiAFg");
	this.shape.setTransform(0.0329,0.0189);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ear_left_Layer_1, null, null);


(lib.body_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Mesh();
	this.instance.parent = this;
	this.instance.setTransform(-35.55,-90.15);

	this.instance_1 = new lib.Mesh_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-35.55,-90.15);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("ASZNlIiCh3QgsgohJgeQhKgeg7gBIqigUQgZgBgRgSQgRgRAAgZIAAiPIn8hGQAAAsgiAcQgiAdgsgJIkMgxQgfgCg4ADQhwAIiBAdQhDAQg9gXQg3gTgLgfIhjADQgrAPgYANQgJAGgaAAQgZABgGAFIhEA6QgOAMgWAEIgQARQgGAHgFAKIgZA2QglAuglAJQgTAEg4gHQgOgBgGgOQgGgNAJgMIDlkqQASgXAfgBQBAgBBPgRQB2gYB9g1QCJg7BNh0QAyhJBMgsQBNgsBXgEIA1gMQA6gKAVAIQAbALAfADIAJAAQAOgHASgGQAfgJATAEQAcAHAhALIE2AAQAUgIAmgLQAjgKAGAAQAzgDAwAUQAlAGA9g7QBBhABtgNQBch1AgjSQAJhegBgmQgBgYAUgbQAVgcAggSQBQgtBJAxQASANALAUQALAVAAAXIABHwIgGAwQgBAJADAJIAHAUQAEAIgCAKIgJAmQgCAJABAKIAGCOQAABBAtAvQAsAwBBADIGBAhIAjgMQAQgFAWAIIAnASIBWAHIApgGQAXgEAKAAQAXAAAkAWIBCACQAhAAAXgHQAjgLAKgBQAogIAaAsQAYArgRAqI0MgJIhaAvQgNAHgFAPQgFAPAHANQAIAOAYAGQAOAFAiAHIDLA5QAPAEAfAGIAkAEIAkAFQANACAaARQAWAOAPAAQAXAAAbAJQAbAJAHAKQASAbAZAHQAcAIAXAUQAeAbALAPQAGAIARAPIAuAlQAUAQgDAaQgCAXgTAUQgSAUgXAFIgNACQgRAAgOgNg");
	this.shape.setTransform(0.0189,2.0302);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.body_Layer_1, null, null);


(lib.tail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_54 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.tail_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(40.9,0.3,1,1,0,0,0,40.9,0.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(55));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(25.9,-83,31.200000000000003,161.7);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(23.6,23.6,1,1,0,0,0,23.6,23.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,47.3,47.2), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(41.3,19.9,1,1,0,0,0,41.3,19.9);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,-2.9,82.6,45.6), null);


(lib.spark_line_fading = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.spark_line("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-51.1,-5.9,1,1,0,0,0,-30.7,-10.6);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:1,startPosition:10},10).wait(35).to({startPosition:45},0).to({alpha:0,startPosition:54},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-91.8,29.8,7.599999999999994,6.199999999999999);


(lib.single_spark_scale_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.single_spark("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.0323,0.0323);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.2,regY:-0.5,scaleX:1,scaleY:1,x:-0.2,y:-0.5},19,cjs.Ease.sineInOut).to({regX:0,regY:0,scaleX:0.0322,scaleY:0.0322,x:0,y:0},20,cjs.Ease.sineInOut).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.9,-16.3,31.200000000000003,31.4);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.tail("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(549.9,534.85,1,1,0,0,0,-7.8,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(109));

}).prototype = p = new cjs.MovieClip();


(lib.mouth = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.mouth_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-48.5,-13.5,1,1,0,0,0,-48.5,-13.5);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.mouth, new cjs.Rectangle(-232.9,-148.9,368.9,271), null);


(lib.mask_mouth_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-59.05,69,1,1,0,0,0,23.6,23.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({scaleX:1.0756,x:-58.8,y:67.15},0).wait(1).to({scaleX:1.1513,x:-58.65,y:65.3},0).wait(1).to({scaleX:1.2269,x:-58.45,y:63.45},0).wait(1).to({scaleX:1.3026,x:-58.25,y:61.6},0).wait(1).to({scaleX:1.3782,x:-58.05,y:59.75},0).wait(1).to({scaleX:1.4538,x:-54.4,y:67.55},0).wait(1).to({scaleX:1.5295,x:-50.65,y:75.35},0).wait(1).to({scaleX:1.6051,x:-46.95,y:83.15},0).wait(1).to({scaleX:1.6808,x:-43.3,y:90.95},0).wait(31).to({scaleX:1.6546,x:-44.75,y:87.8},0).wait(1).to({scaleX:1.6284,x:-46.2,y:84.7},0).wait(1).to({scaleX:1.6022,x:-47.75,y:81.55},0).wait(1).to({scaleX:1.576,x:-49.2,y:78.45},0).wait(1).to({scaleX:1.5498,x:-50.65,y:75.35},0).wait(1).to({scaleX:1.5237,x:-52.15,y:72.2},0).wait(1).to({scaleX:1.4975,x:-53.65,y:69.1},0).wait(1).to({scaleX:1.4713,x:-55.15,y:65.95},0).wait(1).to({scaleX:1.4451,x:-56.6,y:62.85},0).wait(1).to({scaleX:1.4189,x:-58.05,y:59.75},0).wait(1).to({scaleX:1.3351,x:-58.3,y:61.6},0).wait(1).to({scaleX:1.2514,x:-58.45,y:63.45},0).wait(1).to({scaleX:1.1676,x:-58.65,y:65.3},0).wait(1).to({scaleX:1.0838,x:-58.8,y:67.15},0).wait(1).to({scaleX:1,x:-59.05,y:69},0).wait(54).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.mask_mouth_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(-53.75,-21.3,1,1,0,0,0,41.2,21.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:41.3,regY:20.9,scaleX:1.0897,scaleY:1.0915,x:-53.65,y:-30.1},0).wait(1).to({scaleX:1.1795,scaleY:1.183,y:-38.4},0).wait(1).to({scaleX:1.2692,scaleY:1.2745,y:-46.7},0).wait(1).to({scaleX:1.3589,scaleY:1.366,y:-55},0).wait(1).to({scaleX:1.4486,scaleY:1.4575,x:-53.6,y:-63.35},0).wait(1).to({scaleX:1.5384,scaleY:1.549,x:-51.35,y:-71.05},0).wait(1).to({scaleX:1.6281,scaleY:1.6405,x:-49.15,y:-78.7},0).wait(1).to({scaleX:1.7178,scaleY:1.732,x:-46.9,y:-86.45},0).wait(1).to({scaleX:1.8076,scaleY:1.8235,x:-44.7,y:-94.15},0).wait(31).to({scaleX:1.7268,scaleY:1.7411,x:-45.6,y:-86.9},0).wait(1).to({scaleX:1.646,scaleY:1.6588,x:-46.45,y:-79.7},0).wait(1).to({scaleX:1.5653,scaleY:1.5764,x:-47.35,y:-72.45},0).wait(1).to({scaleX:1.4845,scaleY:1.4941,x:-48.25,y:-65.15},0).wait(1).to({scaleX:1.4038,scaleY:1.4117,x:-49.15,y:-57.95},0).wait(1).to({scaleX:1.323,scaleY:1.3294,x:-50.05,y:-50.7},0).wait(1).to({scaleX:1.2423,scaleY:1.247,x:-50.95,y:-43.5},0).wait(1).to({scaleX:1.1615,scaleY:1.1647,x:-51.85,y:-36.25},0).wait(1).to({scaleX:1.0808,scaleY:1.0823,x:-52.7,y:-29.05},0).wait(1).to({scaleX:1,scaleY:1,x:-53.65,y:-21.8},0).wait(59).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.mask_mouth_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.mouth();
	this.instance.parent = this;
	this.instance.setTransform(-41.95,3,1,1,0,0,0,-48.5,-13.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(108).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.mask_mouth = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.mask_mouth_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-59.1,69,1,1,0,0,0,-59.1,69);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1).to({regX:-47.6,regY:75.3,x:-47.6,y:75.3},0).wait(108).to({regX:-59.1,regY:69,x:-59.1,y:69},0).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.mask_mouth_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-53.6,-22.9,1,1,0,0,0,-53.6,-22.9);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1).to({regX:-44.7,regY:-67.8,x:-44.7,y:-67.8},0).wait(108).to({regX:-53.6,regY:-22.9,x:-53.6,y:-22.9},0).wait(1));

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_1 = new cjs.Graphics().p("Ai9AQIAeipQAOAKAOAHQBCAlA2AAQBBAABTg2IA0CpIjZCKg");
	var mask_graphics_2 = new cjs.Graphics().p("Al1AgIA6lOQCkBtCBAAQCBAACjhtIBoFOImuEPg");
	var mask_graphics_3 = new cjs.Graphics().p("AhFF2ImLlUIB2mVIAZgFQC4A9CiAAQBvAAB5gfQAyARA0AJIBqEhIgZBpImcEvg");
	var mask_graphics_4 = new cjs.Graphics().p("AhiGsInZmZICxnbIAYgdQDbAeDRAAQCKAACOgQQAvA8AxA3ICKFXIhRBwIndGEg");
	var mask_graphics_5 = new cjs.Graphics().p("AqmADIEDpWINHAAIEDJWIqnJRg");
	var mask_graphics_6 = new cjs.Graphics().p("ArhhvIEvpaQG3BTGuhTIEuJaQlUG0mNGFQmEluldnLg");
	var mask_graphics_7 = new cjs.Graphics().p("AsbjiIFapdQHMClG3ilIFaJdQlXJAnFHiQm3mzlkpvg");
	var mask_graphics_8 = new cjs.Graphics().p("AtVlVIGFpgQHhD3HAj3IGFJgQlYLNn9I+Qnqn5lssSg");
	var mask_graphics_9 = new cjs.Graphics().p("AuPnHIGvpkQH2FKHLlKIGvJkQlZNYo2KbQoco+l0u1g");
	var mask_graphics_10 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_11 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_12 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_13 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_14 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_15 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_16 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_17 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_18 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_19 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_20 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_21 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_22 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_23 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_24 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_25 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_26 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_27 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_28 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_29 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_30 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_31 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_32 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_33 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_34 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_35 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_36 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_37 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_38 = new cjs.Graphics().p("AuPnIIGvpjQH2FKHLlKIGvJjQlZNZo2KbQoco+l0u2g");
	var mask_graphics_39 = new cjs.Graphics().p("AuPnHIGvpkQH2FKHLlKIGvJkQlZNYo2KbQoco+l0u1g");
	var mask_graphics_40 = new cjs.Graphics().p("AtglsIGNphQHlEJHDkJIGNJhQlYLooJJSQnzoHluszg");
	var mask_graphics_41 = new cjs.Graphics().p("AsykQIFqpeQHWDGG6jGIFrJeQlXJ4ncIHQnLnPlnqwg");
	var mask_graphics_42 = new cjs.Graphics().p("AsDi0IFIpcQHECEGziEIFIJcQlVIImuG9QmjmYlhotg");
	var mask_graphics_43 = new cjs.Graphics().p("ArVhYIEmpZQGzBCGshCIEmJZQlVGYmBFyQl6lglbmqg");
	var mask_graphics_44 = new cjs.Graphics().p("AqmADIEDpWINHAAIEDJWIqnJRg");
	var mask_graphics_45 = new cjs.Graphics().p("Ap1AJIDlohIMhAAIDlIhIp2IQg");
	var mask_graphics_46 = new cjs.Graphics().p("ApCAPIDEnsIL8AAIDFHsIpDHPg");
	var mask_graphics_47 = new cjs.Graphics().p("AoRAUICmm3ILXAAICmG3IoSGQg");
	var mask_graphics_48 = new cjs.Graphics().p("AnfAaICHmCIKxAAICHGCIngFPg");
	var mask_graphics_49 = new cjs.Graphics().p("AmuAgIBolOIKNAAIBoFOImvEPg");
	var mask_graphics_50 = new cjs.Graphics().p("AlYAaIBTkMIIMAAIBSEMIlZDZg");
	var mask_graphics_51 = new cjs.Graphics().p("AkDAUIA+jLIGLAAIA+DLIkECkg");
	var mask_graphics_52 = new cjs.Graphics().p("AiuANIAqiIIEKAAIApCIIivBvg");
	var mask_graphics_53 = new cjs.Graphics().p("AhZAHIAVhGICJAAIAVBGIhaA5g");
	var mask_graphics_54 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_55 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_56 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_57 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_58 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_59 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_60 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_61 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_62 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_63 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_64 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_65 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_66 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_67 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_68 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_69 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_70 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_71 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_72 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_73 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_74 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_75 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_76 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_77 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_78 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_79 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_80 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_81 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_82 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_83 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_84 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_85 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_86 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_87 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_88 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_89 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_90 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_91 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_92 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_93 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_94 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_95 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_96 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_97 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_98 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_99 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_100 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_101 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_102 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_103 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_104 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_105 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_106 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_107 = new cjs.Graphics().p("Ak6D9IAAgGIAJAAIABAGIgFAEg");
	var mask_graphics_108 = new cjs.Graphics().p("AgEABIAAgFIAIAAIABAFIgFAEg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_1,x:-55.9,y:38.6}).wait(1).to({graphics:mask_graphics_2,x:-49.275,y:26.325}).wait(1).to({graphics:mask_graphics_3,x:-52.2,y:13.025}).wait(1).to({graphics:mask_graphics_4,x:-53.575,y:1.45}).wait(1).to({graphics:mask_graphics_5,x:-54.95,y:-10.125}).wait(1).to({graphics:mask_graphics_6,x:-52.05,y:-7.625}).wait(1).to({graphics:mask_graphics_7,x:-49.15,y:-5.125}).wait(1).to({graphics:mask_graphics_8,x:-46.25,y:-2.65}).wait(1).to({graphics:mask_graphics_9,x:-43.3683,y:-0.1419}).wait(1).to({graphics:mask_graphics_10,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_11,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_12,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_13,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_14,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_15,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_16,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_17,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_18,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_19,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_20,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_21,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_22,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_23,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_24,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_25,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_26,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_27,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_28,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_29,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_30,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_31,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_32,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_33,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_34,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_35,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_36,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_37,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_38,x:-43.375,y:-0.15}).wait(1).to({graphics:mask_graphics_39,x:-43.3683,y:-0.1419}).wait(1).to({graphics:mask_graphics_40,x:-45.7,y:-2.15}).wait(1).to({graphics:mask_graphics_41,x:-48,y:-4.15}).wait(1).to({graphics:mask_graphics_42,x:-50.325,y:-6.125}).wait(1).to({graphics:mask_graphics_43,x:-52.625,y:-8.125}).wait(1).to({graphics:mask_graphics_44,x:-54.95,y:-10.125}).wait(1).to({graphics:mask_graphics_45,x:-54.95,y:-2.825}).wait(1).to({graphics:mask_graphics_46,x:-54.95,y:4.45}).wait(1).to({graphics:mask_graphics_47,x:-54.95,y:11.75}).wait(1).to({graphics:mask_graphics_48,x:-54.95,y:19.025}).wait(1).to({graphics:mask_graphics_49,x:-54.95,y:26.325}).wait(1).to({graphics:mask_graphics_50,x:-56.475,y:31.225}).wait(1).to({graphics:mask_graphics_51,x:-57.975,y:36.125}).wait(1).to({graphics:mask_graphics_52,x:-59.525,y:41.05}).wait(1).to({graphics:mask_graphics_53,x:-61.025,y:45.95}).wait(1).to({graphics:mask_graphics_54,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_55,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_56,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_57,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_58,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_59,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_60,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_61,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_62,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_63,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_64,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_65,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_66,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_67,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_68,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_69,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_70,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_71,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_72,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_73,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_74,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_75,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_76,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_77,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_78,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_79,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_80,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_81,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_82,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_83,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_84,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_85,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_86,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_87,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_88,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_89,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_90,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_91,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_92,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_93,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_94,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_95,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_96,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_97,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_98,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_99,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_100,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_101,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_102,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_103,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_104,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_105,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_106,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_107,x:-31.5249,y:25.6748}).wait(1).to({graphics:mask_graphics_108,x:-62.55,y:50.85}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.mask_mouth_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-42,3.1,1,1,0,0,0,-42,3.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(110));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-134.6,-137.7,182.5,252.2);


(lib.head = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.head_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.head, new cjs.Rectangle(-25.8,-21.3,51.6,42.7), null);


(lib.gradient = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween6("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-64.95,-37.45);
	this.instance.alpha = 0;

	this.instance_1 = new lib.Tween9("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-64.95,-37.45);
	this.instance_1.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},16).to({state:[{t:this.instance}]},8).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},4).to({state:[{t:this.instance}]},7).to({state:[{t:this.instance_1}]},18).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:1},16).to({startPosition:0},8).to({startPosition:0},1).to({startPosition:0},4).to({startPosition:0},7).to({_off:true,alpha:0},18).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-193.8,-165.3,257.7,257.70000000000005);


(lib.fire_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.flame("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-173,820.55,0.0094,0.0027,0,0,0,529.4,1731.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:560,regY:1793,scaleX:0.8842,scaleY:0.6018,x:-176.15,y:831.95},4).to({regX:531.2,regY:1772.8,scaleX:1,scaleY:1,x:-179.2,y:819.2},5).to({scaleY:1.0782},2).to({regX:518.6,scaleY:1.1956,y:819.15},3).to({regX:531.3,regY:1773.4,scaleY:0.8841,x:-115.1,y:819.75},5).to({regX:528.1,regY:1773.3,scaleY:0.7646,x:-118.3,y:819.55},5).to({regX:518.6,regY:1772.8,scaleY:1.1956,x:-179.2,y:819.15},5).to({regX:531.2,scaleY:1,y:819.2},5).to({regX:531.1,regY:1769.7,scaleX:0.8842,scaleY:0.6018,x:-201.7,y:817.9},10).to({regX:529.4,regY:1731.5,scaleX:0.0094,scaleY:0.0027,x:-173,y:820.55},5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-746.5,-3406.2,1183.9,4254.7);


(lib.fire = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.flame("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-173,820.55,0.0094,0.0027,0,0,0,529.4,1731.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:531.7,regY:1771.7,scaleX:0.8842,scaleY:0.6018,x:-201.2,y:819.1},4).to({regX:531.2,regY:1772.8,scaleX:1,scaleY:1,x:-179.2,y:819.2},5).to({regX:532,scaleY:1.1956,x:-165.8,y:819.15},5).to({regX:531.6,scaleY:0.8841,x:-114.8,y:819.2},5).to({regX:530.8,scaleY:1.1956,x:-167,y:819.15},5).to({regX:531.2,scaleY:1,x:-179.2,y:819.2},5).to({x:-115.2},5).to({regX:532.4,regY:1771.7,scaleX:0.8842,scaleY:0.6018,x:-200.55,y:819.1},5).to({regX:529.4,regY:1731.5,scaleX:0.0094,scaleY:0.0027,x:-173,y:820.55},5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-731.8,-3406.2,1169.1999999999998,4245.099999999999);


(lib.ear_right = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.ear_right_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.ear_right, new cjs.Rectangle(-15.2,-18.1,30.5,36.3), null);


(lib.ear_left = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.ear_left_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.ear_left, new cjs.Rectangle(-16.2,-18.9,32.5,37.9), null);


(lib.body = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.body_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.body, new cjs.Rectangle(-190.6,-90.1,381.29999999999995,180.3), null);


(lib.spark_flow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.single_spark_scale_2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-83.95,0,1,1,0,0,0,4.5,-0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:-0.2,x:-83.1,y:3.75,startPosition:1},0).wait(1).to({x:-77.45,y:7.35,startPosition:2},0).wait(1).to({x:-71.75,y:10.85,startPosition:3},0).wait(1).to({x:-65.9,y:14.1,startPosition:4},0).wait(1).to({x:-59.95,y:17.15,startPosition:5},0).wait(1).to({x:-53.8,y:19.8,startPosition:6},0).wait(1).to({x:-45.15,y:21.8,startPosition:7},0).wait(1).to({x:-36.3,y:22.5,startPosition:8},0).wait(1).to({x:-27.4,y:22.2,startPosition:9},0).wait(1).to({x:-18.6,y:21,startPosition:10},0).wait(1).to({x:-9.95,y:18.8,startPosition:11},0).wait(1).to({x:-1.7,y:15.5,startPosition:12},0).wait(1).to({x:5.85,y:10.95,startPosition:13},0).wait(1).to({x:12.45,y:5.05,startPosition:14},0).wait(1).to({x:17.65,y:-2.15,startPosition:15},0).wait(1).to({x:20.05,y:-9.3,startPosition:16},0).wait(1).to({x:21.55,y:-16.75,startPosition:17},0).wait(1).to({x:22.7,y:-24.3,startPosition:18},0).wait(1).to({x:23.65,y:-31.85,startPosition:19},0).wait(1).to({x:24.45,y:-39.4,startPosition:20},0).wait(1).to({x:25.1,y:-46.95,startPosition:21},0).wait(1).to({x:25.65,y:-54.55,startPosition:22},0).wait(1).to({x:26.1,y:-62.15,startPosition:23},0).wait(1).to({x:26.4,y:-69.75,startPosition:24},0).wait(1).to({x:27.1,y:-77.6,startPosition:25},0).wait(1).to({x:28.9,y:-85.3,startPosition:26},0).wait(1).to({x:31.7,y:-92.65,startPosition:27},0).wait(1).to({x:35.5,y:-99.6,startPosition:28},0).wait(1).to({x:40.1,y:-106,startPosition:29},0).wait(1).to({x:45.4,y:-111.85,startPosition:30},0).wait(1).to({x:51.45,y:-117.2,startPosition:31},0).wait(1).to({x:57.95,y:-121.45,startPosition:32},0).wait(1).to({x:69.1,y:-125.75,startPosition:33},0).wait(1).to({x:80.9,y:-127.65,startPosition:34},0).wait(1).to({x:92.85,y:-127.3,startPosition:35},0).wait(1).to({x:104.55,y:-124.85,startPosition:36},0).wait(1).to({x:115.8,y:-120.7,startPosition:37},0).wait(1).to({x:126.4,y:-115.15,startPosition:38},0).wait(1).to({x:136.3,y:-108.4,startPosition:39},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88.9,-130,225.9,160.3);


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.mask_mouth("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(733.65,545.25,0.0932,0.0636);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:-43.4,regY:-11.6,x:729.6,y:544.2,startPosition:1},0).wait(1).to({y:543.9,startPosition:2},0).wait(1).to({y:543.65,startPosition:3},0).wait(1).to({y:543.35,startPosition:4},0).wait(1).to({y:543.05,startPosition:5},0).wait(1).to({y:542.8,startPosition:6},0).wait(1).to({y:542.5,startPosition:7},0).wait(1).to({y:542.25,startPosition:8},0).wait(1).to({x:729.65,y:541.85,startPosition:9},0).wait(1).to({x:729.75,y:541.5,startPosition:10},0).wait(1).to({x:729.85,y:541.1,startPosition:11},0).wait(1).to({x:729.9,y:540.75,startPosition:12},0).wait(1).to({x:730,y:540.35,startPosition:13},0).wait(1).to({x:730.1,y:540,startPosition:14},0).wait(1).to({startPosition:15},0).wait(1).to({startPosition:16},0).wait(1).to({startPosition:17},0).wait(1).to({startPosition:18},0).wait(1).to({startPosition:19},0).wait(1).to({y:540.05,startPosition:20},0).wait(1).to({startPosition:21},0).wait(1).to({startPosition:22},0).wait(1).to({startPosition:23},0).wait(1).to({startPosition:24},0).wait(1).to({startPosition:25},0).wait(1).to({y:540.1,startPosition:26},0).wait(1).to({startPosition:27},0).wait(1).to({startPosition:28},0).wait(1).to({startPosition:29},0).wait(1).to({startPosition:30},0).wait(1).to({startPosition:31},0).wait(1).to({y:540.15,startPosition:32},0).wait(1).to({startPosition:33},0).wait(1).to({startPosition:34},0).wait(1).to({startPosition:35},0).wait(1).to({startPosition:36},0).wait(1).to({startPosition:37},0).wait(1).to({y:540.2,startPosition:38},0).wait(1).to({startPosition:39},0).wait(1).to({startPosition:40},0).wait(1).to({startPosition:41},0).wait(1).to({startPosition:42},0).wait(1).to({startPosition:43},0).wait(1).to({y:540.25,startPosition:44},0).wait(1).to({y:540.95,startPosition:45},0).wait(1).to({y:541.65,startPosition:46},0).wait(1).to({y:542.35,startPosition:47},0).wait(1).to({y:543.05,startPosition:48},0).wait(1).to({y:543.75,startPosition:49},0).wait(1).to({y:544.5,startPosition:50},0).wait(1).to({x:729.95,startPosition:51},0).wait(1).to({x:729.85,startPosition:52},0).wait(1).to({x:729.7,startPosition:53},0).wait(1).to({x:729.6,startPosition:54},0).wait(1).to({startPosition:55},0).wait(1).to({startPosition:56},0).wait(1).to({startPosition:57},0).wait(1).to({startPosition:58},0).wait(1).to({startPosition:59},0).wait(1).to({startPosition:60},0).wait(1).to({startPosition:61},0).wait(1).to({startPosition:62},0).wait(1).to({startPosition:63},0).wait(1).to({startPosition:64},0).wait(1).to({startPosition:65},0).wait(1).to({startPosition:66},0).wait(1).to({startPosition:67},0).wait(1).to({startPosition:68},0).wait(1).to({startPosition:69},0).wait(1).to({startPosition:70},0).wait(1).to({startPosition:71},0).wait(1).to({startPosition:72},0).wait(1).to({startPosition:73},0).wait(1).to({startPosition:74},0).wait(1).to({startPosition:75},0).wait(1).to({startPosition:76},0).wait(1).to({startPosition:77},0).wait(1).to({startPosition:78},0).wait(1).to({startPosition:79},0).wait(1).to({startPosition:80},0).wait(1).to({startPosition:81},0).wait(1).to({startPosition:82},0).wait(1).to({startPosition:83},0).wait(1).to({startPosition:84},0).wait(1).to({startPosition:85},0).wait(1).to({startPosition:86},0).wait(1).to({startPosition:87},0).wait(1).to({startPosition:88},0).wait(1).to({startPosition:89},0).wait(1).to({startPosition:90},0).wait(1).to({startPosition:91},0).wait(1).to({startPosition:92},0).wait(1).to({startPosition:93},0).wait(1).to({startPosition:94},0).wait(1).to({startPosition:95},0).wait(1).to({startPosition:96},0).wait(1).to({startPosition:97},0).wait(1).to({startPosition:98},0).wait(1).to({startPosition:99},0).wait(1).to({startPosition:100},0).wait(1).to({startPosition:101},0).wait(1).to({startPosition:102},0).wait(1).to({startPosition:103},0).wait(1).to({startPosition:104},0).wait(1).to({startPosition:105},0).wait(1).to({startPosition:106},0).wait(1).to({startPosition:107},0).wait(1).to({startPosition:108},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.gradient("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(726.15,551.4,0.5005,0.5005,0,0,0,-64.8,-37.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(43).to({startPosition:47},0).to({alpha:0,startPosition:51},6).wait(60));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.ear_left();
	this.instance.parent = this;
	this.instance.setTransform(731.85,532.1,1,1,0,0,0,16.3,18.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:0,scaleY:0.9902,rotation:-1.267,x:715.3019,y:513.4709},0).wait(1).to({scaleX:0.9999,scaleY:0.9805,rotation:-2.534,x:715.0701,y:513.7506},0).wait(1).to({scaleX:0.9998,scaleY:0.9707,rotation:-3.801,x:714.8549,y:514.0386},0).wait(1).to({scaleY:0.9609,rotation:-5.068,x:714.6565,y:514.3345},0).wait(1).to({scaleX:0.9997,scaleY:0.9511,rotation:-6.335,x:714.4749,y:514.6377},0).wait(1).to({scaleX:0.9996,scaleY:0.9413,rotation:-7.602,x:714.3103,y:514.9479},0).wait(1).to({scaleX:0.9995,scaleY:0.9315,rotation:-8.869,x:714.1629,y:515.2646},0).wait(1).to({scaleY:0.9218,rotation:-10.136,x:714.0328,y:515.5872},0).wait(1).to({scaleX:0.9994,scaleY:0.912,rotation:-11.4031,x:713.9201,y:515.9153},0).wait(1).to({scaleY:0.9022,rotation:-12.6701,x:713.825,y:516.2485},0).wait(1).to({scaleX:0.9993,scaleY:0.8924,rotation:-13.9371,x:713.7474,y:516.5863},0).wait(1).to({scaleX:0.9992,scaleY:0.8826,rotation:-15.2041,x:713.6874,y:516.9282},0).wait(1).to({scaleY:0.8728,rotation:-16.4711,x:713.6452,y:517.2736},0).wait(1).to({scaleX:0.9991,scaleY:0.8631,rotation:-17.7381,x:713.6208,y:517.6222},0).wait(1).to({scaleX:0.999,scaleY:0.8533,rotation:-19.0051,x:713.6142,y:517.9734},0).wait(1).to({scaleY:0.8435,rotation:-20.2721,x:713.6254,y:518.3268},0).wait(1).to({scaleX:0.9989,scaleY:0.8337,rotation:-21.5391,x:713.6545,y:518.6819},0).wait(18).to({scaleX:0.999,scaleY:0.842,rotation:-20.4621,x:713.6287,y:518.38},0).wait(1).to({scaleY:0.8504,rotation:-19.3852,x:713.6157,y:518.0793},0).wait(1).to({scaleX:0.9991,scaleY:0.8587,rotation:-18.3082,x:713.6156,y:517.78},0).wait(1).to({scaleY:0.867,rotation:-17.2313,x:713.6284,y:517.4824},0).wait(1).to({scaleX:0.9992,scaleY:0.8753,rotation:-16.1543,x:713.6541,y:517.1869},0).wait(1).to({scaleY:0.8836,rotation:-15.0774,x:713.6926,y:516.8938},0).wait(1).to({scaleX:0.9993,scaleY:0.8919,rotation:-14.0004,x:713.7439,y:516.6033},0).wait(1).to({scaleX:0.9994,scaleY:0.9002,rotation:-12.9235,x:713.808,y:516.3157},0).wait(1).to({scaleY:0.9086,rotation:-11.8465,x:713.8848,y:516.0314},0).wait(1).to({scaleX:0.9995,scaleY:0.9169,rotation:-10.7695,x:713.9743,y:515.7506},0).wait(1).to({scaleY:0.9252,rotation:-9.6926,x:714.0764,y:515.4736},0).wait(1).to({scaleX:0.9996,scaleY:0.9335,rotation:-8.6156,x:714.191,y:515.2007},0).wait(1).to({scaleY:0.9418,rotation:-7.5387,x:714.3181,y:514.9322},0).wait(1).to({scaleX:0.9997,scaleY:0.9501,rotation:-6.4617,x:714.4576,y:514.6684},0).wait(1).to({scaleY:0.9584,rotation:-5.3848,x:714.6095,y:514.4096},0).wait(1).to({scaleX:0.9998,scaleY:0.9667,rotation:-4.3078,x:714.7735,y:514.156},0).wait(1).to({scaleX:0.9999,scaleY:0.9751,rotation:-3.2309,x:714.9497,y:513.908},0).wait(1).to({scaleY:0.9834,rotation:-2.1539,x:715.1379,y:513.6658},0).wait(1).to({scaleX:1,scaleY:0.9917,rotation:-1.077,x:715.3381,y:513.4297},0).wait(1).to({scaleY:1,rotation:0,x:715.55,y:513.2},0).wait(55));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.ear_right();
	this.instance.parent = this;
	this.instance.setTransform(742.7,530.95,1,1,0,0,0,-15.2,18.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:0,rotation:1.4292,x:758.4744,y:513.0171},0).wait(1).to({rotation:2.8583,x:759.0391,y:513.2953},0).wait(1).to({rotation:4.2875,x:759.5938,y:513.5842},0).wait(1).to({rotation:5.7167,x:760.1382,y:513.8837},0).wait(1).to({rotation:7.1459,x:760.6721,y:514.1936},0).wait(1).to({rotation:8.575,x:761.1952,y:514.5134},0).wait(1).to({rotation:10.0042,x:761.7072,y:514.843},0).wait(1).to({rotation:11.4334,x:762.208,y:515.1821},0).wait(1).to({rotation:12.8625,x:762.6972,y:515.5303},0).wait(1).to({rotation:14.2917,x:763.1748,y:515.8875},0).wait(1).to({rotation:15.7209,x:763.6404,y:516.2533},0).wait(1).to({rotation:17.15,x:764.0938,y:516.6274},0).wait(1).to({rotation:18.5792,x:764.5348,y:517.0094},0).wait(1).to({rotation:20.0084,x:764.9633,y:517.3992},0).wait(1).to({rotation:21.4376,x:765.3791,y:517.7964},0).wait(1).to({rotation:22.8667,x:765.7819,y:518.2006},0).wait(1).to({rotation:24.2959,x:766.1716,y:518.6115},0).wait(18).to({rotation:23.0811,x:765.8412,y:518.2618},0).wait(1).to({rotation:21.8663,x:765.5013,y:517.9169},0).wait(1).to({rotation:20.6515,x:765.152,y:517.5771},0).wait(1).to({rotation:19.4367,x:764.7934,y:517.2424},0).wait(1).to({rotation:18.2219,x:764.4257,y:516.9132},0).wait(1).to({rotation:17.0071,x:764.049,y:516.5896},0).wait(1).to({rotation:15.7923,x:763.6633,y:516.2718},0).wait(1).to({rotation:14.5775,x:763.2689,y:515.96},0).wait(1).to({rotation:13.3627,x:762.8657,y:515.6543},0).wait(1).to({rotation:12.148,x:762.4541,y:515.3551},0).wait(1).to({rotation:10.9332,x:762.034,y:515.0623},0).wait(1).to({rotation:9.7184,x:761.6057,y:514.7763},0).wait(1).to({rotation:8.5036,x:761.1693,y:514.4972},0).wait(1).to({rotation:7.2888,x:760.7249,y:514.2251},0).wait(1).to({rotation:6.074,x:760.2727,y:513.9602},0).wait(1).to({rotation:4.8592,x:759.8128,y:513.7028},0).wait(1).to({rotation:3.6444,x:759.3454,y:513.4529},0).wait(1).to({rotation:2.4296,x:758.8707,y:513.2107},0).wait(1).to({rotation:1.2148,x:758.3889,y:512.9763},0).wait(1).to({rotation:0,x:757.9,y:512.75},0).wait(55));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.head();
	this.instance.parent = this;
	this.instance.setTransform(736.85,537.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({scaleX:1.0053,scaleY:0.9853,x:736.9679,y:536.9446},0).wait(1).to({scaleX:1.0106,scaleY:0.9706,x:737.0853,y:536.6901},0).wait(1).to({scaleX:1.0159,scaleY:0.956,x:737.2022,y:536.437},0).wait(1).to({scaleX:1.0212,scaleY:0.9415,x:737.3184,y:536.185},0).wait(1).to({scaleX:1.0264,scaleY:0.927,x:737.4342,y:535.9342},0).wait(1).to({scaleX:1.0316,scaleY:0.9127,x:737.5494,y:535.6847},0).wait(1).to({scaleX:1.0368,scaleY:0.8983,x:737.664,y:535.4364},0).wait(1).to({scaleX:1.042,scaleY:0.8841,x:737.7781,y:535.1892},0).wait(1).to({scaleX:1.0471,scaleY:0.8699,x:737.8916,y:534.9432},0).wait(1).to({scaleX:1.0522,scaleY:0.8558,x:738.0046,y:534.6985},0).wait(1).to({scaleX:1.0573,scaleY:0.8418,x:738.117,y:534.4549},0).wait(1).to({scaleX:1.0623,scaleY:0.8278,x:738.2288,y:534.2127},0).wait(1).to({scaleX:1.0674,scaleY:0.8139,x:738.34,y:533.9716},0).wait(1).to({scaleX:1.0678,scaleY:0.8127,x:738.3499,y:533.9503},0).wait(1).to({x:738.3498,y:533.9505},0).wait(1).to({x:738.3496,y:533.9508},0).wait(1).to({x:738.3495,y:533.9511},0).wait(1).to({x:738.3494,y:533.9514},0).wait(1).to({x:738.3492,y:533.9516},0).wait(1).to({scaleY:0.8128,x:738.3491,y:533.9519},0).wait(1).to({x:738.349,y:533.9522},0).wait(1).to({x:738.3489,y:533.9525},0).wait(1).to({x:738.3488,y:533.9527},0).wait(1).to({x:738.3486,y:533.953},0).wait(1).to({x:738.3485,y:533.9533},0).wait(1).to({scaleY:0.8129,x:738.3484,y:533.9535},0).wait(1).to({scaleX:1.0677,x:738.3483,y:533.9538},0).wait(1).to({x:738.3481,y:533.954},0).wait(1).to({x:738.348,y:533.9543},0).wait(1).to({x:738.3479,y:533.9546},0).wait(1).to({x:738.3478,y:533.9548},0).wait(1).to({x:738.3477,y:533.9551},0).wait(1).to({scaleY:0.813,x:738.3476,y:533.9553},0).wait(1).to({x:738.3474,y:533.9556},0).wait(1).to({x:738.3473,y:533.9558},0).wait(1).to({x:738.3472,y:533.9561},0).wait(1).to({x:738.3471,y:533.9563},0).wait(1).to({scaleX:1.0645,scaleY:0.8218,x:738.277,y:534.1081},0).wait(1).to({scaleX:1.0552,scaleY:0.8474,x:738.0717,y:534.5529},0).wait(1).to({scaleX:1.046,scaleY:0.8729,x:737.8676,y:534.9952},0).wait(1).to({scaleX:1.0368,scaleY:0.8983,x:737.6646,y:535.4351},0).wait(1).to({scaleX:1.0277,scaleY:0.9235,x:737.4627,y:535.8726},0).wait(1).to({scaleX:1.0186,scaleY:0.9486,x:737.2619,y:536.3076},0).wait(1).to({scaleX:1.0096,scaleY:0.9735,x:737.0624,y:536.7398},0).wait(1).to({scaleX:1.0006,scaleY:0.9982,x:736.864,y:537.1696},0).wait(1).to({scaleX:1,scaleY:1,x:736.85,y:537.2},0).wait(63));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.body();
	this.instance.parent = this;
	this.instance.setTransform(678.8,619.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(109));

}).prototype = p = new cjs.MovieClip();


(lib.fire_bigger = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.fire_2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-65,1671.6,0.4977,0.4977,-34.4832,0,0,-154.6,-1280);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},50).wait(5));

	// Layer_11
	this.instance_1 = new lib.fire_2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(492,1500.7,1,0.5798,0,0,0,-154.6,-1280.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).wait(51));

	// Layer_10
	this.instance_2 = new lib.fire_2("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(930.75,1680.7,0.6864,0.3938,33.7061,0,0,-154.5,-1280.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({_off:true},50).wait(3));

	// Layer_9
	this.instance_3 = new lib.fire("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(95,1394.7,0.5836,0.6871,-16.2049,0,0,-154.6,-1280.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({_off:true},45).wait(10));

	// Layer_8
	this.instance_4 = new lib.fire_2("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(808.5,1132.9,0.6121,0.6121,8.7297,0,0,-154.4,-1280);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({_off:true},50).wait(5));

	// Layer_4
	this.instance_5 = new lib.fire("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(479.2,286.1,1,1,0,0,0,-154.6,-1280.2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(2).to({_off:false},0).to({_off:true},45).wait(8));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-668.2,-1839.9,2057.6000000000004,4633.200000000001);


(lib.fire_big_scale = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.fire_bigger("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(2482.8,-533.35,0.309,0.1712,0,0,0,544.4,2778.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:416.1,regY:517.6,scaleX:0.3762,scaleY:0.2236,x:2434.55,y:-1038.7,startPosition:1},0).wait(1).to({scaleX:0.4433,scaleY:0.2759,x:2425.95,y:-1157.05,startPosition:2},0).wait(1).to({scaleX:0.5105,scaleY:0.3283,x:2417.3,y:-1275.4,startPosition:3},0).wait(1).to({scaleX:0.5777,scaleY:0.3806,x:2408.65,y:-1393.75,startPosition:4},0).wait(1).to({scaleX:0.6449,scaleY:0.433,x:2400.05,y:-1512.1,startPosition:5},0).wait(1).to({scaleX:0.712,scaleY:0.4853,x:2391.45,y:-1630.45,startPosition:6},0).wait(1).to({scaleX:0.7792,scaleY:0.5377,x:2382.85,y:-1748.8,startPosition:7},0).wait(1).to({scaleX:0.8464,scaleY:0.59,x:2374.2,y:-1867.15,startPosition:8},0).wait(1).to({scaleX:0.9135,scaleY:0.6424,x:2365.6,y:-1985.45,startPosition:9},0).wait(1).to({scaleX:0.9807,scaleY:0.6947,x:2357,y:-2103.8,startPosition:10},0).wait(1).to({scaleX:0.9788,scaleY:0.6929,x:2356.55,y:-2103,startPosition:11},0).wait(1).to({scaleX:0.9769,scaleY:0.6912,x:2356.15,y:-2102.15,startPosition:12},0).wait(1).to({scaleX:0.9749,scaleY:0.6894,x:2355.7,y:-2101.3,startPosition:13},0).wait(1).to({scaleX:0.973,scaleY:0.6876,x:2355.3,y:-2100.5,startPosition:14},0).wait(1).to({scaleX:0.9711,scaleY:0.6859,x:2354.85,y:-2099.65,startPosition:15},0).wait(1).to({scaleX:0.9691,scaleY:0.6841,x:2354.4,y:-2098.85,startPosition:16},0).wait(1).to({scaleX:0.9672,scaleY:0.6823,x:2354,y:-2098,startPosition:17},0).wait(1).to({scaleX:0.9653,scaleY:0.6805,x:2353.55,y:-2097.15,startPosition:18},0).wait(1).to({scaleX:0.9633,scaleY:0.6788,x:2353.15,y:-2096.35,startPosition:19},0).wait(1).to({scaleX:0.9614,scaleY:0.677,x:2352.7,y:-2095.5,startPosition:20},0).wait(1).to({scaleX:0.9595,scaleY:0.6752,x:2352.3,y:-2094.6,startPosition:21},0).wait(1).to({scaleX:0.9534,scaleY:0.6895,x:2352.35,y:-2130.15,startPosition:22},0).wait(1).to({scaleX:0.9474,scaleY:0.7038,x:2352.5,y:-2165.7,startPosition:23},0).wait(1).to({scaleX:0.9413,scaleY:0.7182,x:2352.55,y:-2201.2,startPosition:24},0).wait(1).to({scaleX:0.9352,scaleY:0.7325,x:2352.7,y:-2236.75,startPosition:25},0).wait(1).to({scaleX:0.9292,scaleY:0.7468,x:2352.8,y:-2272.25,startPosition:26},0).wait(1).to({scaleX:0.9231,scaleY:0.7611,x:2352.85,y:-2307.75,startPosition:27},0).wait(1).to({scaleX:0.9171,scaleY:0.7754,x:2353,y:-2343.3,startPosition:28},0).wait(1).to({scaleX:0.911,scaleY:0.7897,x:2353.05,y:-2378.85,startPosition:29},0).wait(1).to({scaleX:0.9049,scaleY:0.804,x:2353.2,y:-2414.35,startPosition:30},0).wait(1).to({scaleX:0.8989,scaleY:0.8183,x:2353.25,y:-2449.9,startPosition:31},0).wait(1).to({scaleX:0.8928,scaleY:0.8326,x:2353.4,y:-2485.4,startPosition:32},0).wait(1).to({scaleX:0.8868,scaleY:0.847,x:2353.5,y:-2520.9,startPosition:33},0).wait(1).to({scaleX:0.8807,scaleY:0.8613,x:2353.6,y:-2556.45,startPosition:34},0).wait(1).to({scaleX:0.8746,scaleY:0.8756,x:2353.7,y:-2592,startPosition:35},0).wait(1).to({scaleX:0.8686,scaleY:0.8899,x:2353.8,y:-2627.5,startPosition:36},0).wait(1).to({scaleX:0.8625,scaleY:0.9042,x:2353.9,y:-2663.05,startPosition:37},0).wait(1).to({scaleX:0.8564,scaleY:0.9185,x:2353.95,y:-2698.6,startPosition:38},0).wait(1).to({scaleX:0.8504,scaleY:0.9328,x:2354.1,y:-2734.05,startPosition:39},0).wait(1).to({scaleX:0.8443,scaleY:0.9471,x:2354.15,y:-2769.6,startPosition:40},0).wait(1).to({scaleX:0.8383,scaleY:0.9614,x:2354.3,y:-2805.15,startPosition:41},0).wait(1).to({scaleX:0.8322,scaleY:0.9758,x:2354.4,y:-2840.65,startPosition:42},0).wait(1).to({scaleX:0.8261,scaleY:0.9901,x:2354.5,y:-2876.2,startPosition:43},0).wait(1).to({scaleX:0.7103,scaleY:0.8502,x:2368.65,y:-2563.25,startPosition:44},0).wait(1).to({scaleX:0.5945,scaleY:0.7104,x:2382.85,y:-2250.4,startPosition:45},0).wait(1).to({scaleX:0.4787,scaleY:0.5706,x:2397,y:-1937.45,startPosition:46},0).wait(1).to({scaleX:0.3628,scaleY:0.4308,x:2411.2,y:-1624.55,startPosition:47},0).wait(1).to({scaleX:0.247,scaleY:0.291,x:2425.35,y:-1311.6,startPosition:48},0).wait(1).to({scaleX:0.1312,scaleY:0.1512,x:2439.55,y:-998.7,startPosition:49},0).wait(1).to({startPosition:50},0).wait(1).to({startPosition:51},0).wait(1).to({startPosition:52},0).wait(1).to({startPosition:53},0).wait(1).to({startPosition:54},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1300.3,-4110.1,1997.3999999999999,3581.8);


(lib.fire_group = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_15
	this.instance = new lib.fire_big_scale("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(704.75,572.05,0.0207,0.0207,0,-145.4676,34.5324,2355.1,-2256.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(55));

	// Layer_14
	this.instance_1 = new lib.fire_big_scale("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(720.45,584.45,0.0324,0.0258,-165.0008,0,0,2343.7,-2245.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(55));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(684.5,540.7,70.70000000000005,89.39999999999998);


(lib.fire_with_sparks = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.spark_flow("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-72.25,-4.2,0.0857,0.0857,135,0,0,26.4,-49.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({_off:false},0).wait(45));

	// Layer_5
	this.instance_1 = new lib.spark_flow("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-18.2,4.15,0.1039,0.1039,0,-173.042,6.958,26,-50.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10).to({_off:false},0).wait(45));

	// Layer_4
	this.instance_2 = new lib.spark_line_fading("synched",10);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-50.7,-12.6,1,1,104.999,0,0,-51,-5.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off:true},45).wait(10));

	// Layer_2
	this.instance_3 = new lib.spark_flow("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(-41.7,7,0.1072,0.1072,104.9976,0,0,0.1,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({_off:true},40).wait(15));

	// Layer_1
	this.instance_4 = new lib.fire_group("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(-46.15,1.85,1,1,0,0,0,718.9,584.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(45).to({startPosition:47},0).to({alpha:0,startPosition:49},5).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-81.1,-61.1,73.69999999999999,108.30000000000001);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.fire_with_sparks("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(721.65,571.7,0.7818,0.7818,0,0,0,-46.8,-0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:-54.9,regY:11.6,x:715.35,y:581.3,startPosition:1},0).wait(1).to({y:581.25,startPosition:2},0).wait(1).to({y:581.2,startPosition:3},0).wait(1).to({y:581.1,startPosition:4},0).wait(1).to({y:581.05,startPosition:5},0).wait(1).to({y:581,startPosition:6},0).wait(1).to({y:580.95,startPosition:7},0).wait(1).to({y:580.85,startPosition:8},0).wait(1).to({y:580.8,startPosition:9},0).wait(1).to({y:580.75,startPosition:10},0).wait(1).to({y:580.65,startPosition:11},0).wait(1).to({y:580.6,startPosition:12},0).wait(1).to({y:580.55,startPosition:13},0).wait(1).to({y:580.5,startPosition:14},0).wait(1).to({y:580.4,startPosition:15},0).wait(1).to({y:580.35,startPosition:16},0).wait(1).to({y:580.3,startPosition:17},0).wait(1).to({y:580.2,startPosition:18},0).wait(1).to({y:580.15,startPosition:19},0).wait(1).to({y:580.1,startPosition:20},0).wait(1).to({y:580.05,startPosition:21},0).wait(1).to({y:579.95,startPosition:22},0).wait(1).to({y:579.9,startPosition:23},0).wait(1).to({y:579.85,startPosition:24},0).wait(1).to({y:579.75,startPosition:25},0).wait(1).to({y:579.7,startPosition:26},0).wait(1).to({y:579.65,startPosition:27},0).wait(1).to({y:579.6,startPosition:28},0).wait(1).to({y:579.5,startPosition:29},0).wait(1).to({y:579.45,startPosition:30},0).wait(1).to({y:579.4,startPosition:31},0).wait(1).to({y:579.3,startPosition:32},0).wait(1).to({y:579.25,startPosition:33},0).wait(1).to({y:579.2,startPosition:34},0).wait(1).to({y:579.15,startPosition:35},0).wait(1).to({y:579.05,startPosition:36},0).wait(1).to({y:579,startPosition:37},0).wait(1).to({y:578.95,startPosition:38},0).wait(1).to({y:578.9,startPosition:39},0).wait(1).to({y:579.15,startPosition:40},0).wait(1).to({y:579.4,startPosition:41},0).wait(1).to({y:579.65,startPosition:42},0).wait(1).to({y:579.9,startPosition:43},0).wait(1).to({y:580.15,startPosition:44},0).wait(1).to({y:580.4,startPosition:45},0).wait(1).to({y:580.65,startPosition:46},0).wait(1).to({alpha:0.75,startPosition:47},0).wait(1).to({alpha:0.5,startPosition:48},0).wait(1).to({alpha:0.25,startPosition:49},0).wait(1).to({alpha:0,startPosition:50},0).wait(1).to({startPosition:51},0).wait(1).to({startPosition:52},0).wait(1).to({startPosition:53},0).wait(1).to({startPosition:54},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:1},0).wait(1).to({startPosition:2},0).wait(1).to({startPosition:3},0).wait(1).to({startPosition:4},0).wait(1).to({startPosition:5},0).wait(1).to({startPosition:6},0).wait(1).to({startPosition:7},0).wait(1).to({startPosition:8},0).wait(1).to({startPosition:9},0).wait(1).to({startPosition:10},0).wait(1).to({startPosition:11},0).wait(1).to({startPosition:12},0).wait(1).to({startPosition:13},0).wait(1).to({startPosition:14},0).wait(1).to({startPosition:15},0).wait(1).to({startPosition:16},0).wait(1).to({startPosition:17},0).wait(1).to({startPosition:18},0).wait(1).to({startPosition:19},0).wait(1).to({startPosition:20},0).wait(1).to({startPosition:21},0).wait(1).to({startPosition:22},0).wait(1).to({startPosition:23},0).wait(1).to({startPosition:24},0).wait(1).to({startPosition:25},0).wait(1).to({startPosition:26},0).wait(1).to({startPosition:27},0).wait(1).to({startPosition:28},0).wait(1).to({startPosition:29},0).wait(1).to({startPosition:30},0).wait(1).to({startPosition:31},0).wait(1).to({startPosition:32},0).wait(1).to({startPosition:33},0).wait(1).to({startPosition:34},0).wait(1).to({startPosition:35},0).wait(1).to({startPosition:36},0).wait(1).to({startPosition:37},0).wait(1).to({startPosition:38},0).wait(1).to({startPosition:39},0).wait(1).to({startPosition:40},0).wait(1).to({startPosition:41},0).wait(1).to({startPosition:42},0).wait(1).to({startPosition:43},0).wait(1).to({startPosition:44},0).wait(1).to({startPosition:45},0).wait(1).to({startPosition:46},0).wait(1).to({startPosition:47},0).wait(1).to({startPosition:48},0).wait(1).to({startPosition:49},0).wait(1).to({startPosition:50},0).wait(1).to({startPosition:51},0).wait(1).to({startPosition:52},0).wait(1).to({startPosition:53},0).wait(1));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.manarola_fire = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_108 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(108).call(this.frame_108).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(718.6,562.2,1,1,0,0,0,718.6,562.2);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1).to({regX:715.3,regY:580.2,x:715.3,y:580.2},0).wait(108));

	// Layer_9 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EA1gAlbICKkxIB+G8Ih8MNg");
	var mask_graphics_1 = new cjs.Graphics().p("EAyfAlbIEQkxID5G8Ij0MNg");
	var mask_graphics_2 = new cjs.Graphics().p("EAvcAlbIGZkxIF0G8IluMNg");
	var mask_graphics_3 = new cjs.Graphics().p("EAsbAlbIIfkxIHvG8InmMNg");
	var mask_graphics_4 = new cjs.Graphics().p("EApZAlcIKmkyIJrG8IpgMOg");
	var mask_graphics_5 = new cjs.Graphics().p("EAmXAlcIMukyILmG8IrZMOg");
	var mask_graphics_6 = new cjs.Graphics().p("EAjVAlcIO1kyINhG8ItSMOg");
	var mask_graphics_7 = new cjs.Graphics().p("EAgTAlcIQ9kyIPcG8IvLMOg");
	var mask_graphics_8 = new cjs.Graphics().p("EAgTAlcIQ9kyIPcG8IvLMOg");
	var mask_graphics_9 = new cjs.Graphics().p("EAgTAlcIQ9kyIPcG8IvLMOg");
	var mask_graphics_10 = new cjs.Graphics().p("EAgTAlcIQ9kyIPcG8IvLMOg");
	var mask_graphics_11 = new cjs.Graphics().p("EAgTAlcIQ9kyIPcG8IvLMOg");
	var mask_graphics_12 = new cjs.Graphics().p("EAgTAlcIQ9kyIPcG8IvLMOg");
	var mask_graphics_13 = new cjs.Graphics().p("EAiEAlcIPukyIOVG8IuFMOg");
	var mask_graphics_14 = new cjs.Graphics().p("EAj1AlcIOfkyINNG9Is+MNg");
	var mask_graphics_15 = new cjs.Graphics().p("EAlmAlcINQkyIMFG9Ir3MNg");
	var mask_graphics_16 = new cjs.Graphics().p("EAj1AlcIOfkyINMG9Is9MNg");
	var mask_graphics_17 = new cjs.Graphics().p("EAiDAlcIPukxIOVG8IuFMNg");
	var mask_graphics_18 = new cjs.Graphics().p("EAgTAlcIQ9kxIPcG8IvLMNg");
	var mask_graphics_19 = new cjs.Graphics().p("EAgTAlcIQ9kxIPcG8IvLMNg");
	var mask_graphics_20 = new cjs.Graphics().p("EAgTAldIQ9kyIPcG8IvLMNg");
	var mask_graphics_21 = new cjs.Graphics().p("EAgTAldIQ9kyIPcG8IvLMNg");
	var mask_graphics_22 = new cjs.Graphics().p("EAjlAldIOqkyINXG8ItIMNg");
	var mask_graphics_23 = new cjs.Graphics().p("EAm3AldIMYkyILRG8IrFMNg");
	var mask_graphics_24 = new cjs.Graphics().p("EAjlAldIOqkyINXG8ItIMNg");
	var mask_graphics_25 = new cjs.Graphics().p("EAgTAldIQ9kyIPcG8IvLMNg");
	var mask_graphics_26 = new cjs.Graphics().p("EAgTAldIQ9kyIPcG8IvLMOg");
	var mask_graphics_27 = new cjs.Graphics().p("EAgTAldIQ9kyIPcG8IvLMOg");
	var mask_graphics_28 = new cjs.Graphics().p("EAgTAldIQ9kyIPcG9IvLMNg");
	var mask_graphics_29 = new cjs.Graphics().p("EAhKAldIQWkxIO6G8IupMNg");
	var mask_graphics_30 = new cjs.Graphics().p("EAiCAldIPvkxIOWG8IuGMNg");
	var mask_graphics_31 = new cjs.Graphics().p("EAi5AldIPJkxINzG8ItkMNg");
	var mask_graphics_32 = new cjs.Graphics().p("EAiCAldIPvkxIOWG8IuGMNg");
	var mask_graphics_33 = new cjs.Graphics().p("EAhKAldIQWkyIO5G9IuoMNg");
	var mask_graphics_34 = new cjs.Graphics().p("EAgTAldIQ9kxIPcG8IvLMNg");
	var mask_graphics_35 = new cjs.Graphics().p("EAgTAldIQ9kxIPcG8IvLMNg");
	var mask_graphics_36 = new cjs.Graphics().p("EAgTAleIQ9kyIPcG8IvLMNg");
	var mask_graphics_37 = new cjs.Graphics().p("EAgTAleIQ9kyIPcG8IvLMNg");
	var mask_graphics_38 = new cjs.Graphics().p("EAgTAleIQ9kyIPcG8IvLMOg");
	var mask_graphics_39 = new cjs.Graphics().p("AwLk3IQ7koIPcGyIvLMNg");
	var mask_graphics_40 = new cjs.Graphics().p("EAkHAlbIOSknINCGyIszMNg");
	var mask_graphics_41 = new cjs.Graphics().p("EAn7AlbILoknIKmGyIqaMNg");
	var mask_graphics_42 = new cjs.Graphics().p("EArvAlbII+knIILGyIoCMNg");
	var mask_graphics_43 = new cjs.Graphics().p("EAvjAlbIGUknIFwGyIlqMNg");
	var mask_graphics_44 = new cjs.Graphics().p("EAwxAlbIFdknIE/GyIk5MNg");
	var mask_graphics_45 = new cjs.Graphics().p("EAyAAlbIEmknIEMGyIkHMNg");
	var mask_graphics_46 = new cjs.Graphics().p("EAzPAlbIDvknIDaGyIjWMNg");
	var mask_graphics_47 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_48 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_49 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_50 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_51 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_52 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_53 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_54 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_55 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_56 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_57 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_58 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_59 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_60 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_61 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_62 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_63 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_64 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_65 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_66 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_67 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_68 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_69 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_70 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_71 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_72 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_73 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_74 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_75 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_76 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_77 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_78 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_79 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_80 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_81 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_82 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_83 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_84 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_85 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_86 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_87 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_88 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_89 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_90 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_91 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_92 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_93 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_94 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_95 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_96 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_97 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_98 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_99 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_100 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_101 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_102 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_103 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_104 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_105 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_106 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_107 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");
	var mask_graphics_108 = new cjs.Graphics().p("EA0eAlbIC4knICoGyIilMNg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:368.7512,y:331.525}).wait(1).to({graphics:mask_graphics_1,x:375.2143,y:331.525}).wait(1).to({graphics:mask_graphics_2,x:381.654,y:331.525}).wait(1).to({graphics:mask_graphics_3,x:388.1429,y:331.525}).wait(1).to({graphics:mask_graphics_4,x:394.6067,y:331.55}).wait(1).to({graphics:mask_graphics_5,x:401.0706,y:331.55}).wait(1).to({graphics:mask_graphics_6,x:407.5353,y:331.55}).wait(1).to({graphics:mask_graphics_7,x:414,y:331.55}).wait(1).to({graphics:mask_graphics_8,x:414,y:331.55}).wait(1).to({graphics:mask_graphics_9,x:414,y:331.55}).wait(1).to({graphics:mask_graphics_10,x:414,y:331.575}).wait(1).to({graphics:mask_graphics_11,x:414,y:331.575}).wait(1).to({graphics:mask_graphics_12,x:414,y:331.575}).wait(1).to({graphics:mask_graphics_13,x:410.2609,y:331.575}).wait(1).to({graphics:mask_graphics_14,x:406.4967,y:331.6}).wait(1).to({graphics:mask_graphics_15,x:402.7084,y:331.6}).wait(1).to({graphics:mask_graphics_16,x:406.4459,y:331.6}).wait(1).to({graphics:mask_graphics_17,x:410.1843,y:331.625}).wait(1).to({graphics:mask_graphics_18,x:414,y:331.625}).wait(1).to({graphics:mask_graphics_19,x:414,y:331.625}).wait(1).to({graphics:mask_graphics_20,x:414,y:331.65}).wait(1).to({graphics:mask_graphics_21,x:414,y:331.65}).wait(1).to({graphics:mask_graphics_22,x:407,y:331.65}).wait(1).to({graphics:mask_graphics_23,x:399.9999,y:331.65}).wait(1).to({graphics:mask_graphics_24,x:406.9984,y:331.65}).wait(1).to({graphics:mask_graphics_25,x:414,y:331.65}).wait(1).to({graphics:mask_graphics_26,x:414,y:331.675}).wait(1).to({graphics:mask_graphics_27,x:414,y:331.675}).wait(1).to({graphics:mask_graphics_28,x:414,y:331.7}).wait(1).to({graphics:mask_graphics_29,x:412.1583,y:331.725}).wait(1).to({graphics:mask_graphics_30,x:410.3407,y:331.725}).wait(1).to({graphics:mask_graphics_31,x:408.4748,y:331.725}).wait(1).to({graphics:mask_graphics_32,x:410.3149,y:331.725}).wait(1).to({graphics:mask_graphics_33,x:412.1325,y:331.7}).wait(1).to({graphics:mask_graphics_34,x:414,y:331.725}).wait(1).to({graphics:mask_graphics_35,x:414,y:331.725}).wait(1).to({graphics:mask_graphics_36,x:414,y:331.75}).wait(1).to({graphics:mask_graphics_37,x:414,y:331.75}).wait(1).to({graphics:mask_graphics_38,x:414,y:331.775}).wait(1).to({graphics:mask_graphics_39,x:724.35,y:602.275}).wait(1).to({graphics:mask_graphics_40,x:405.8707,y:331.525}).wait(1).to({graphics:mask_graphics_41,x:397.7406,y:331.525}).wait(1).to({graphics:mask_graphics_42,x:389.6113,y:331.525}).wait(1).to({graphics:mask_graphics_43,x:381.4813,y:331.525}).wait(1).to({graphics:mask_graphics_44,x:378.8543,y:331.525}).wait(1).to({graphics:mask_graphics_45,x:376.2023,y:331.525}).wait(1).to({graphics:mask_graphics_46,x:373.6011,y:331.525}).wait(1).to({graphics:mask_graphics_47,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_48,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_49,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_50,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_51,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_52,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_53,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_54,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_55,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_56,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_57,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_58,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_59,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_60,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_61,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_62,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_63,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_64,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_65,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_66,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_67,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_68,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_69,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_70,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_71,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_72,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_73,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_74,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_75,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_76,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_77,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_78,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_79,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_80,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_81,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_82,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_83,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_84,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_85,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_86,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_87,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_88,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_89,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_90,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_91,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_92,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_93,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_94,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_95,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_96,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_97,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_98,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_99,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_100,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_101,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_102,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_103,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_104,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_105,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_106,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_107,x:370.9749,y:331.525}).wait(1).to({graphics:mask_graphics_108,x:370.9749,y:331.525}).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(726.1,551.9,1,1,0,0,0,726.1,551.9);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 1
	this.Layer_8.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_8];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(109));

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(729.8,545.4,1,1,0,0,0,729.8,545.4);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 2
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(1).to({regX:729.9,regY:542.3,x:729.9,y:542.3},0).wait(108));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(715.6,513.2,1,1,0,0,0,715.6,513.2);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 3
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1).to({regX:713.7,regY:516.8,x:713.7,y:516.8},0).wait(108));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(757.9,512.8,1,1,0,0,0,757.9,512.8);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 4
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1).to({regX:765.1,regY:517.9,x:765.1,y:517.9},0).wait(108));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(736.9,537.2,1,1,0,0,0,736.9,537.2);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 5
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1).to({regX:738.4,x:738.4},0).wait(108));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(678.8,619.9,1,1,0,0,0,678.8,619.9);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 6
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(109));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(581.7,535.1,1,1,0,0,0,581.7,535.1);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 7
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(109));

	// Layer_1_obj_
	this.Layer_1 = new lib.Scene_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(640.9,427.7,1,1,0,0,0,640.9,427.7);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 8
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(109));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(636,424.5,649.9000000000001,431.5);
// library properties:
lib.properties = {
	id: '91861DA1DD064A9C992336D048DF2475',
	width: 1280,
	height: 850,
	fps: 24,
	color: "#000033",
	opacity: 1.00,
	manifest: [
		{src:"images/manarola_fire_atlas_.png?1553005830017", id:"manarola_fire_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['91861DA1DD064A9C992336D048DF2475'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;